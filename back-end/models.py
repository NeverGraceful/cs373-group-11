from sqlalchemy import Column, Table, ForeignKey, create_engine, Index
from sqlalchemy.orm import relationship, sessionmaker, declarative_base
from sqlalchemy.types import Integer, String, Float, Text
from Database import engine
import pylint

Base = declarative_base()

#assoc table for countries and news
countries_news = Table(
    "Countries_News",
    Base.metadata,
    Column("country_code", ForeignKey("Countries1.code"), primary_key=True),
    Column("news_id", ForeignKey("News.id"), primary_key=True),
)

#assoc table for news and orgs
news_orgs = Table(
    "News_Orgs",
    Base.metadata,
    Column("news_id", ForeignKey("News.id"), primary_key=True),
    Column("org_name", ForeignKey("Orgs.short_name")),
)


class Countries1(Base):

  __tablename__ = "Countries1"

  id = Column(Integer, autoincrement=True)
  name = Column(String(255), nullable=False)
  code = Column(String(3), unique=True, primary_key=True)
  population = Column(Integer)
  refugees = Column(Integer)
  asylum_seekers = Column(Integer)
  distance = Column(Float)
  flag_url = Column(String(255))
  chart = Column(String(2000))

  news = relationship("News", secondary=countries_news, back_populates="countries")

  def __repr__(self):
    return f"<Country: {self.name}>"
  
class News(Base):

  __tablename__ = "News"

  id = Column(Integer, primary_key=True, unique=True, autoincrement=False)
  title = Column(String(1000), nullable=False)
  date = Column(String(255))
  # source = Column(String(255))
  image = Column(String(255))
  # countries = Column(String(2000))
  type = Column(String(255))
  text = Column(Text)
  link = Column(String(255))
  map = Column(String(255))

  countries = relationship("Countries1", secondary=countries_news, back_populates="news")
  orgs = relationship("Orgs", secondary=news_orgs, back_populates="news")
  

  def __repr__(self):
    return f"<Article: {self.title}>"
  
class Orgs(Base):

  __tablename__ = "Orgs"
  
  id = Column(Integer, primary_key=True, unique=True, autoincrement=False)
  name = Column(String(255))
  short_name = Column(String(255), index=True)
  image = Column(String(255))
  homepage = Column(String(255))
  date_created = Column(String(255))
  status = Column(String(255))
  # country = Column(String(255))
  type = Column(String(255))
  description = Column(Text)
  country_code = Column(String(3), ForeignKey("Countries1.code"))
  
  news = relationship("News", secondary=news_orgs, back_populates="orgs")
  

  def __repr__(self):
    return f"<Organization: {self.name}>"


Base.metadata.create_all(engine)