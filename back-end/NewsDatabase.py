from database import session
import json
from models import News, countries_news, news_orgs
import requests
from urllib3.exceptions import HTTPError
from sqlalchemy import insert

def main():
    ## connect to the database

    response = requests.get(f"https://api.reliefweb.int/v1/reports?appname=sss&query[value]\
                =South Sudan&query[fields][]=country&fields[include][]=source.shortname&\
                fields[include][]=country.iso3&fields[include][]=primary_country&\
                fields[include][]=date&fields[include][]=image&preset=latest&query[operator]=AND&\
                fields[include][]=id&fields[include][]=format.name&fields[include][]=language&\
                fields[include][]=body&fields[include][]=origin&offset=0&limit=200")
  
    response_data = response.json()
    reports = response_data["data"]

    images = None
    try:
        # subscription_key = "98a7793be7fd4b28a75af3aef2995331"
        # subscription_key = "e69b958dd5e9425ea4fd65e330139470"
        subscription_key = "bec12c5860884a85ad647686f866f42c"
        search_url = "https://api.bing.microsoft.com/v7.0/images/search"
        search_term = "South Sudan Crisis"
        headers = {"Ocp-Apim-Subscription-Key" : subscription_key}
        params = {"q": search_term, "license":"any", "count":"200", "offset":"0"}
        response = requests.get(search_url, headers=headers, params=params)
        response.raise_for_status()
        search_results = response.json()
        images = [img["thumbnailUrl"] for img in search_results["value"]]
        print(images)
        get_image = iter(images)
        
    except HTTPError as http_err:
        error_message = f'HTTP error occurred: {http_err}'
        print(error_message)

    except Exception as err:
        error_message = f'An error occurred: {err}'
        print(error_message)
  
    for report in reports:
        fields = report["fields"]
        

        news_map = [0, 0]
        primary_country = fields.get("primary_country")
        GOOGLE_MAPS_API_KEY = 'AIzaSyAhA375sjTU9u3bFuxcY68_r3mge6VaVHg'
        if primary_country:
            geocode_url = f"https://maps.googleapis.com/maps/api/geocode/json?address={primary_country}&key={GOOGLE_MAPS_API_KEY}"
            geocode_response = requests.get(geocode_url)
            geocode_data = geocode_response.json()
            
            if geocode_data['status'] == 'OK':
                # Fetch latitude / longitude
                latitude = geocode_data['results'][0]['geometry']['location']['lat']
                longitude = geocode_data['results'][0]['geometry']['location']['lng']
                
                # Fetch Google Maps link for country
                # maps_link = f"https://www.google.com/maps/search/?api=1&query={latitude},{longitude}"
                # # print(maps_link)
                news_map = {"lat":latitude, "long": longitude}

        # Bind parameters securely
        news_record = News(
          id=fields.get("id"), 
          title=fields.get("title"), 
          date=fields.get("date").get("created"), 
          image=next(get_image) if images else "", 
          type=fields.get("format")[0]["name"] if isinstance(fields.get("format"), list) else fields.get("format").get("name", ""),
          text=fields.get("body") if fields.get("body") else "", 
          link=fields.get("origin"), 
          map=json.dumps(news_map)
        )

        try:
            session.add(news_record)
            session.commit()
            print(f"{fields.get('id')} inserted into News!")
        except Exception as e:
            session.rollback()
            print(e)
            print("***************END*********************")
        
        countries = [country.get("iso3").upper() for country in fields.get("country", [])]
        sources = [source.get("shortname") for source in fields.get("source", [])]
        ##update association table for news-countries
        for code in countries:
          if(code == "WLD"):
            pass
          stmt = insert(countries_news).values(country_code=code, news_id=fields.get("id"))
          try:
            session.execute(stmt)
          except Exception as e:
            session.rollback()
            print(e)
        #update association table for news-orgs
        for source in sources:
          stmt = insert(news_orgs).values(org_name=source, news_id=fields.get("id"))
          try:
            session.execute(stmt)
          except Exception as e:
            session.rollback()
            print(e)
    session.close()

if __name__ == "__main__":
  main()