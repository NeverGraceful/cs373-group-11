from operator import concat
import os
import subprocess
import ssl
import certifi
import base64
import requests
import time
from datetime import datetime

from models import countries_news, news_orgs, Countries1, Orgs, News

from flask import *
from flask_cors import CORS
from sqlalchemy import Column, Integer, String, Text, create_engine, select, Table, MetaData, text, case, func, Float, ForeignKey, or_
from sqlalchemy.orm import sessionmaker, aliased
from requests.exceptions import HTTPError
import json
from Database import session, engine

metadata = MetaData()

GOOGLE_MAPS_API_KEY = 'AIzaSyAhA375sjTU9u3bFuxcY68_r3mge6VaVHg'

Countries = Table(
    'Countries', metadata,
    Column('id', Integer, primary_key=True, autoincrement=True),
    Column('name', String(255), nullable=False),
    Column('code', String(3), unique=True, primary_key=True),
    Column('population', Integer),
    Column('refugees', Integer),
    Column('asylum_seekers', Integer),
    Column('distance', Float),
    Column('flag_url', String(255)),
    Column('chart', String(2000)),
    autoload_with=engine
)

# News_Old = Table('News', metadata, autoload_with=engine)

Organizations = Table(
    'Orgs', metadata,
    Column('id', Integer, primary_key=True, unique=True, autoincrement=False),
    Column('name', String(255)),
    Column('short_name', String(255), index=True),
    Column('image', String(255)),
    Column('homepage', String(255)),
    Column('date_created', String(255)),
    Column('status', String(255)),
    Column('type', String(255)),
    Column('description', Text),
    Column('country_code', String(3), ForeignKey("Countries1.code")),
    autoload_with=engine
)

News_Old = Table(
    'News', metadata,
    Column('id', Integer, primary_key=True),
    Column('title', String(255)),
    Column('date', String(255)),
    Column('image', String(255)),
    Column('text', Text),
    Column('link', String(255)),
    Column('map', Text),
    Column('type', String(255)),
    autoload_with=engine,
    extend_existing=True  # Add this line to allow redefinition
)

app = Flask(__name__)
CORS(app)

model = []

"""
Countries - GET Endpoints
"""
# Route for getting ALL countries
@app.route('/countries')
def get_countries():
    page = int(request.args.get("page")) if request.args.get("page") else 1
    limit = int(request.args.get("limit")) if request.args.get("limit") else 1000
    search = request.args.get("search", "")
    sort = request.args.get("sort", "").lower()

    sort_map = {
        "name": Countries1.name.asc(),
        "refugees": Countries1.refugees.desc(),
        "asylum_seekers": Countries1.asylum_seekers.desc(),
        "population": Countries1.population.desc(),
        "distance": Countries1.distance.asc()
    }

    if sort :
        order_by_clause = sort_map[sort]
        
    stmt = (
        select(
            Countries1.id,
            Countries1.name,
            Countries1.code,
            Countries1.population,
            Countries1.refugees,
            Countries1.asylum_seekers,
            Countries1.distance,
            Countries1.flag_url,
            Countries1.chart,
            func.group_concat(News.title).label('news_titles'),
            func.group_concat(News.id).label('news_id'),
            func.group_concat(Orgs.name).label('org_names'),
            func.group_concat(Orgs.short_name).label('org_short_names')
        )
        .outerjoin(countries_news, Countries1.code == countries_news.c.country_code)
        .outerjoin(News, countries_news.c.news_id == News.id)
        .outerjoin(news_orgs, News.id == news_orgs.c.news_id)
        .outerjoin(Orgs, or_(Countries1.code == Orgs.country_code, news_orgs.c.org_name == Orgs.short_name))
        .group_by(
            Countries1.id,
            Countries1.name,
            Countries1.code,
            Countries1.population,
            Countries1.refugees,
            Countries1.asylum_seekers,
            Countries1.distance,
            Countries1.flag_url,
            Countries1.chart
        )
    )

    if search:
        if sort:
            stmt = stmt.filter(
                or_(
                    Countries1.name.ilike(f"%{search}%"),
                    Countries1.code.ilike(f"%{search}%"),
                    Orgs.name.ilike(f"%{search}%"),
                    Orgs.short_name.ilike(f"%{search}%"),
                    News.title.ilike(f"%{search}%")
                )
            ).order_by(order_by_clause)
        else:
            stmt = stmt.filter(
                or_(
                    Countries1.name.ilike(f"%{search}%"),
                    Countries1.code.ilike(f"%{search}%"),
                    Orgs.name.ilike(f"%{search}%"),
                    Orgs.short_name.ilike(f"%{search}%"),
                    News.title.ilike(f"%{search}%")
                )
            )
    elif sort:
        # Apply sorting scheme to full country set query
        stmt = stmt.order_by(order_by_clause)
        
    total_results = session.execute(stmt)
    num_total_results = len(total_results.fetchall())

    stmt = stmt.offset((page - 1) * limit).limit(limit)
    result = session.execute(stmt)
    all_rows = result.fetchall()

    md = {
        "page": page,
        "limit": limit,
        "total": num_total_results,
        "search": search if search else ""
    }

    json_data = []
    for row in all_rows:
        row_dict = {
            'name': row[1],
            'code': row[2],
            'population': row[3],
            'refugees': row[4],
            'asylum_seekers': row[5],
            'distance': row[6],
            'flag_url': row[7],
            'chart': row[8],
            'news_titles': row[9].split('*,') if row[9] else [],
            'news_id': row[10].split(',') if row[10] else [],
            'org_names': row[11].split(',') if row[11] else [],
            'org_short_names': row[12].split(',') if row[12] else []
        }
        try:
            if row[8]:
                row_dict['chart'] = json.loads(row[8])
        except json.JSONDecodeError as e:  # Correct the exception name
            print(f"Error decoding JSON: {e}")
        json_data.append(row_dict)
            
    return jsonify({"_metadata" : md, "countries" : json_data}) 

# Route for getting SINGLE country
@app.route('/countries/<string:name>')
def get_country(name):
    stmt = (
        select(
            Countries1.id,
            Countries1.name,
            Countries1.code,
            Countries1.population,
            Countries1.refugees,
            Countries1.asylum_seekers,
            Countries1.distance,
            Countries1.flag_url,
            Countries1.chart,
            func.group_concat(News.title).label('news_titles'),
            func.group_concat(News.id).label('news_id'),
            func.group_concat(Orgs.name).label('org_names'),
            func.group_concat(Orgs.short_name).label('org_short_names')
        )
        .outerjoin(countries_news, Countries1.code == countries_news.c.country_code)
        .outerjoin(News, countries_news.c.news_id == News.id)
        .outerjoin(news_orgs, News.id == news_orgs.c.news_id)
        .outerjoin(Orgs, or_(Countries1.code == Orgs.country_code, news_orgs.c.org_name == Orgs.short_name))
        .group_by(
            Countries1.id,
            Countries1.name,
            Countries1.code,
            Countries1.population,
            Countries1.refugees,
            Countries1.asylum_seekers,
            Countries1.distance,
            Countries1.flag_url,
            Countries1.chart
        )
        .where(Countries1.name == name)
    )

    with engine.connect() as connection:
        connection.execute(text("SET SESSION sort_buffer_size = 3145728;"))
        result = connection.execute(stmt)
        country = result.fetchone()

    if country:
        country_data = {
            'name': country[1],
            'code': country[2],
            'population': country[3],
            'refugees': country[4],
            'asylum_seekers': country[5],
            'distance': country[6],
            'flag_url': country[7],
            'chart': country[8],
            'news_titles': country[9].split('*,') if country[9] else [],
            'news_id': country[10].split(',') if country[10] else [],
            'org_names': country[11].split(',') if country[11] else [],
            'org_short_names': country[12].split(',') if country[12] else []
        }
        try:
            if country[8]:
                country_data['chart'] = json.loads(country[8])
        except json.JSONDecodeError as e:
            print(f"Error decoding JSON: {e}")
        return jsonify(country_data)

    return jsonify({'error': 'Country not found'})


"""
Orgs - GET Endpoints
"""
# Route for getting ALL organizations
@app.route('/orgs')
def get_orgs():
    if(request.args.get("page")):
        page = int(request.args.get("page"))
    else:
        page = 1
    if(request.args.get("limit")):
        limit = int(request.args.get("limit"))
    else:
        limit = 10

    search = request.args.get("search", "")
    sort = request.args.get("sort", "").lower()
    status = request.args.get("status", default="", type=str)
    org_type = request.args.get("type", default="", type=str)


    sort_map = {
        "id": Orgs.id.asc(),
        "name": Orgs.name.asc(),
        "short_name": Orgs.short_name.asc(),
        "date_created": Orgs.date_created.desc() 
    }

    if sort :
        order_by_clause = sort_map[sort]

    stmt = (
        select(
            Countries1.name,
            Orgs,
            func.group_concat(concat(News.title, '*')).label('news_titles'),
            func.group_concat(News.id).label('news_ids')
        )
        .join(Orgs, Countries1.code == Orgs.country_code)
        .outerjoin(news_orgs, Orgs.short_name == news_orgs.c.org_name)
        .outerjoin(News, news_orgs.c.news_id == News.id)
        .group_by(Countries1.name, Orgs)
    )

    if search:
        if sort:
            stmt = stmt.filter(
                or_(
                    Orgs.name.ilike(f"%{search}%"),
                    Orgs.short_name.ilike(f"%{search}%"),
                    Orgs.homepage.ilike(f"%{search}%"),
                    Orgs.type.ilike(f"%{search}%"),
                    Orgs.description.ilike(f"%{search}%"),
                    Orgs.country_code.ilike(f"%{search}%"),
                    Countries1.name.ilike(f"%{search}%"),
                    News.title.ilike(f"%{search}%")
                )
            ).order_by(order_by_clause)
        else:
            stmt = stmt.filter(
                or_(
                    Orgs.name.ilike(f"%{search}%"),
                    Orgs.short_name.ilike(f"%{search}%"),
                    Orgs.homepage.ilike(f"%{search}%"),
                    Orgs.type.ilike(f"%{search}%"),
                    Orgs.description.ilike(f"%{search}%"),
                    Orgs.country_code.ilike(f"%{search}%"),
                    Countries1.name.ilike(f"%{search}%"),
                    News.title.ilike(f"%{search}%")
                )
            )
    elif sort:
        stmt = stmt.order_by(order_by_clause)

    if status:
        stmt = stmt.filter(Orgs.status == status)
    if org_type:
        stmt = stmt.filter(Orgs.type.ilike(f"%{org_type}%"))

    total_results = session.execute(stmt)
    num_total_results = len(total_results.fetchall())
        
    with engine.connect() as conn: #doesn't work with session.execute idk why
        conn.execute(text("SET SESSION sort_buffer_size = 3145728;"))
        stmt = stmt.offset((page - 1) * limit).limit(limit)
        result = conn.execute(stmt)
        orgs = result.fetchall()
    
    md = {
        "page": page,
        "limit": limit,
        "total": num_total_results,
        "search": search if search else ""
    }

    json_data = []
    for org in orgs:
        org_data = {
            "id": org[1],
            "name": org[2],
            "short_name": org[3],
            "image": org[4],
            "homepage": org[5],
            "date_created": org[6],
            "status": org[7],
            "type": org[8],
            "description": org[9],
            "country_code": org[10],
            "news": org[11].split('*,') if org[11] else [],
            "news_id": org[12].split('*,') if org[12] else [],
            "country": [org[0]],
        }
        json_data.append(org_data)

    return jsonify({"_metadata" : md, "orgs": json_data})

# Route for getting SINGLE organization
@app.route('/orgs/<string:name>')
def get_org(name):
    stmt = (
        select(
            Countries1.name,
            Orgs,
            func.group_concat(concat(News.title, '*')).label('news_titles'),
            func.group_concat(News.id).label('news_ids')
        )
        .join(Orgs, Countries1.code == Orgs.country_code)
        .outerjoin(news_orgs, Orgs.short_name == news_orgs.c.org_name)
        .outerjoin(News, news_orgs.c.news_id == News.id)
        .where(Orgs.name == name)
        .group_by(Countries1.name, Orgs)
    )

    with engine.connect() as conn:
        result = conn.execute(stmt)
        orgs = result.fetchall()
        
    for org in orgs:   
        if org[2] == name:
            org_data = {
                "id": org[1],
                "name": org[2],
                "short_name": org[3],
                "image": org[4],
                "homepage": org[5],
                "date_created": org[6],
                "status": org[7],
                "type": org[8],
                "description": org[9],
                "country_code": org[10],
                "news": org[11].split('*,') if org[11] else [],
                "news_id": org[12].split('*,') if org[12] else [],
                "country": [org[0]],
            }
            return json.dumps(org_data)
    
    return json.dumps({'error': 'Org not found'})

"""
News - GET Endpoints
"""
# Route for getting all news
@app.route('/news')
def get_news():
    if(request.args.get("page")):
        page = int(request.args.get("page"))
    else:
        page = 1
    if(request.args.get("limit")):
        limit = int(request.args.get("limit"))
    else:
        limit = 1000
    search = request.args.get("search", "")
    sort = request.args.get("sort", "").lower()

    source = request.args.get("source", default="", type=str)
    country_name = request.args.get("country_name", default="", type=str)
    news_type = request.args.get("type", default="", type=str)

    sort_map = {
        "id": News.id.asc(),
        "title": News.title.asc(),
        "date": News.date.desc()
    }

    if sort :
        order_by_clause = sort_map[sort]

    stmt = (
            select(func.group_concat(concat(Countries1.name,"*")).label('country_names'), func.group_concat(concat(Countries1.code,"*")).label('country_codes'), Orgs.name, Orgs.short_name, News)
            .join(countries_news, News.id == countries_news.c.news_id)
            .join(Countries1, countries_news.c.country_code == Countries1.code)
            .outerjoin(news_orgs, News.id == news_orgs.c.news_id)
            .outerjoin(Orgs, news_orgs.c.org_name == Orgs.short_name)
            .group_by(News, Orgs.name, Orgs.short_name)
        )

    if search:
        if sort:
            stmt = stmt.filter(
                or_(
                    News.title.ilike(f"%{search}%"),
                    News.type.ilike(f"%{search}%"),
                    News.text.ilike(f"%{search}%"),
                    Countries1.name.ilike(f"%{search}%"),
                    Orgs.short_name.ilike(f"%{search}%"),
                    Orgs.description.ilike(f"%{search}%")
                )
            ).order_by(order_by_clause)
        else:
            stmt = stmt.filter(
                or_(
                    News.title.ilike(f"%{search}%"),
                    News.type.ilike(f"%{search}%"),
                    News.text.ilike(f"%{search}%"),
                    Countries1.name.ilike(f"%{search}%"),
                    Orgs.short_name.ilike(f"%{search}%"),
                    Orgs.description.ilike(f"%{search}%")
                )
            )
    # Apply sorting scheme to full country set query
    elif sort:
        stmt = stmt.order_by(order_by_clause)

    # Filter on news type if specified in query
    if news_type:
        stmt = stmt.filter(News.type.ilike(f"%{news_type}%"))

    # Filter on country name if specified in query
    if country_name:
        stmt = stmt.filter(News.countries.any(Countries1.name == country_name))
    
    # Filter on specific news source if specified in query
    if source:
        stmt = stmt.filter(News.countries.any(source == news_orgs.c.org_name))


    with engine.connect() as conn: #buffer size to small for session
        conn.execute(text("SET SESSION sort_buffer_size = 3145728;"))
        total_results = conn.execute(stmt)
        num_total_results = len(total_results.fetchall())

        stmt = stmt.offset((page - 1) * limit).limit(limit)
        result = conn.execute(stmt)
        news = result.fetchall()
    
    
    md = {
        "page": page,
        "limit": limit,
        "total": num_total_results,
        "search": search if search else ""
    }

    json_data  = []
    news_dict = {}
    
    for n in news:
        news_title = n[5]
    
        if news_title not in news_dict:
            news_dict[news_title] = {
                'id': n[4],
                'Title': n[5],
                'Date': datetime.strptime(n[6], "%Y-%m-%dT%H:%M:%S%z").strftime("%m-%d-%Y"),
                'Sources': [n[3]],
                'Sources_Short': [n[2]],  
                'Image': n[7],
                'Countries': n[0].split('*,') if news[0] else [],
                'Countries_Code': n[1].split('*,') if news[0] else [],
                'Type': n[8],
                'Text': n[9],
                'Link': n[10],
                'Map': json.loads(n[11])
            }
        json_data = list(news_dict.values())
    return jsonify({"_metadata" : md, "news": json_data})

# Route for getting single news instance
@app.route('/news/<int:id>')
def get_article(id):
    stmt = (
            select(func.group_concat(concat(Countries1.name,"*")).label('country_names'), func.group_concat(concat(Countries1.code,"*")).label('country_codes'), Orgs.name, Orgs.short_name, News)
            .join(countries_news, News.id == countries_news.c.news_id)
            .join(Countries1, countries_news.c.country_code == Countries1.code)
            .outerjoin(news_orgs, News.id == news_orgs.c.news_id)
            .outerjoin(Orgs, news_orgs.c.org_name == Orgs.short_name)
            .group_by(News, Orgs.name, Orgs.short_name)
        ) 
    
    with engine.connect() as connection:
        connection.execute(text("SET SESSION sort_buffer_size = 3145728;"))
        result = connection.execute(stmt)
        articles = result.fetchall()

    for article in articles:
        if article[4] == id:
            article_data = {
                'id': article[4],
                'Title': article[5],
                'Date': datetime.strptime(article[6], "%Y-%m-%dT%H:%M:%S%z").strftime("%m-%d-%Y"),
                'Sources': [article[3]],
                'Sources_Short': [article[2]],  
                'Image': article[7],
                'Countries': article[0].split('*,') if article[0] else [],
                'Countries_Code': article[1].split('*,') if article[0] else [],
                'Type': article[8],
                'Text': article[9],
                'Link': article[10],
                'Map': json.loads(article[11])
            }
            return json.dumps(article_data)
    
    return json.dumps({'error': 'Article not found'})


if __name__ == "__main__":
    app.run(debug=True, port=9000)