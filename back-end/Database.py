"""Database engine & session creation."""
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from dotenv import load_dotenv
import os
import certifi

load_dotenv()

dialect = "mysql"
driver = "pymysql"
username = os.getenv('DB_USERNAME')
password = os.getenv('DB_PASSWORD')
host = os.getenv('DB_HOST')
port = 3306
database = os.getenv('DB_NAME')

ssl_context = {
    "ssl": "VERIFY_IDENTITY",
    "ssl_ca": certifi.where(),
}
connection_url = f"{dialect}+{driver}://{username}:{password}@{host}:{port}/{database}"

engine = create_engine(connection_url, connect_args=ssl_context)

Session = sessionmaker(bind=engine)
session = Session()
