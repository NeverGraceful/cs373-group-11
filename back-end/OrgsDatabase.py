from database import session, engine
import json
from models import Orgs, News
import requests
import time
from urllib3.exceptions import HTTPError
from sqlalchemy import select


def main():

  stmt = select(News)
  all_sources = []
    
  with engine.connect() as connection:
        result = connection.execute(stmt)
        all_news = result.fetchall()
  
  for row in all_news:

    
    id = row[0]
    print(f"getting orgs for news {id}")

    response = requests.get(f"https://api.reliefweb.int/v1/reports?appname=apidoc&query[value]\
                ={id}&query[fields][]=id&fields[include][]=source.shortname&limit=1")
    
    response_data = response.json()
    news = response_data["data"]

    for new in news:
       
      fields = new["fields"]

      sources = [source.get("shortname") for source in fields.get("source", [])]

      for org in sources:

        res_orgs = requests.get(f"https://api.reliefweb.int/v1/sources?appname=apidoc&query[value]\
                ={org}&query[fields][]=shortname&fields[include][]=name&fields[include][]\
                =country.name&fields[include][]=type.name&fields[include][]=date.created&\
                fields[include][]=description&fields[include][]=status&fields[include][]=homepage&\
                fields[include][]=id&fields[include][]=country.iso3&fields[include][]=shortname&\
                fields[include][]=logo.url&limit=1")
        
        response_data = res_orgs.json()
        print("Appending org JSON!")
        all_sources.extend(response_data["data"])
        print(response_data["data"])


  # get south sudan orgs
  response = requests.get(f"https://api.reliefweb.int/v1/sources?appname=apidoc&query[value]\
              =South Sudan&query[fields][]=country&fields[include][]=name&fields[include][]\
              =country.name&fields[include][]=type.name&fields[include][]=date.created&\
              fields[include][]=description&fields[include][]=status&fields[include][]=homepage&\
              fields[include][]=country.iso3&fields[include][]=shortname&fields[include][]=id&\
              fields[include][]=logo.url&limit=100")

  response_data = response.json()
  all_sources.extend(response_data["data"])

  

  for source in all_sources:
    print(source)
    fields = source["fields"]

    image = fields.get("logo").get("url") if fields.get("logo") else None

    if not image:
      try:
        subscription_key = "bec12c5860884a85ad647686f866f42c"
        search_url = "https://api.bing.microsoft.com/v7.0/images/search"
        search_term = fields["name"]
        headers = {"Ocp-Apim-Subscription-Key" : subscription_key}
        params = {"q": search_term, "license":"any", "count":"1", "offset":"0"}
        time.sleep(0.35)
        response = requests.get(search_url, headers=headers, params=params)
        response.raise_for_status()
        search_results = response.json()
        image = search_results["value"][0]["thumbnailUrl"]

      except HTTPError as http_err:
        error_message = f'HTTP error occurred: {http_err}'
        print(error_message)

      except Exception as err:
        error_message = f'An error occurred: {err}'
        print(error_message)

    # Bind parameters securely
    org_record = Orgs(
      id=fields.get("id"), 
      name=fields["name"],
      short_name=fields["shortname"],
      image=image if image else "",
      homepage=fields.get("homepage"),
      date_created=fields.get("date").get("created"), 
      status = fields["status"],
      type=fields["type"]["name"],
      description = fields.get("description"),
      country_code=fields.get("country", [])[0].get("iso3").upper()
    )

    try:
        session.add(org_record)
        session.commit()
        print(f"{fields.get('id')} inserted into Orgs!")
    except Exception as e:
        session.rollback()
        print(e)
        print("***************END*********************")
    
  session.close()

if __name__ == "__main__":
  main()