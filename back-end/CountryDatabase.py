import Country
from database import session
import json
from restcountries import RestCountryApiV2 as rest_countries
from models import Countries1

def main():

  countries = rest_countries.get_all(filters=["alpha3Code"])
  
  for cr in countries:
    country = Country.Country("", cr.alpha3_code) 
    if country.update_country():

      country_record = Countries1(
        name=country.name,
        code = country.code,
        population = country.population,
        refugees = country.refugees,
        asylum_seekers = country.asylum_seekers,
        distance = country.distance,
        flag_url = country.flag,
        chart = json.dumps(country.chart)
      )

      
      try:
        session.add(country_record)
        print(f"{country.code} inserted into Countries!")
      except Exception as e:
        print(f"{country.code} already exists in Countries!")

  session.commit()
  session.close()

if __name__ == "__main__":
  main()
