from restcountries import RestCountryApiV2 as rest_countries
from geopy import distance
import requests
import matplotlib.pyplot as plt

class Country:

  global unhcr_api
  unhcr_api = "https://api.unhcr.org/population/v1/population/"

  def __init__(self, name: str, code: str):
    self.name = name
    self.code = code
    self.population = 0
    self.refugees = 0
    self.asylum_seekers = 0
    self.distance = 0
    self.flag = None
    self.chart = {}

  def update_country(self):
    ss_coords = [7.0, 30.0] #coordinates of South Sudan
    try:
      country = rest_countries.get_country_by_country_code(self.code)
      self.name = country.name
      self.code = country.alpha3_code
      self.population = country.population
      self.distance = distance.distance(ss_coords, country.latlng).miles
      self.distance = round(self.distance, 2)
      self.flag = country.flag

      parameters = {
        "coo": "SSD",
        "coa": self.code,
        "cf_type": "ISO"
      }
      
      response = requests.get(unhcr_api, params=parameters)
      self.__handle_refugee_data(response.json())
  
    except Exception as e:
      print("**Cannot resolve " + self.code + "**")
      print(e)
      return False
    return True
  
  def __handle_refugee_data(self, response):
    data = response['items']

    for entry in data:
      refugees = int(entry['refugees'])
      asylum_seekers = int(entry['asylum_seekers'])
      self.chart[int(entry['year'])] = [refugees, asylum_seekers]

      self.refugees += refugees
      self.asylum_seekers += asylum_seekers
    
    # fig, ax = plt.subplots()
    # width = 0.35
    # ax.set_title("South Sudanese refugees in " + self.name)
    # ax.set_xlabel("Years")
    # ax.set_ylabel("Refugees and Asylum Seekers")
    # ax.bar(years_x, refugees_y, width, label="Refugees", align="center")
    # ax.bar([p + width for p in years_x], asylum_y, width, label="Asylum Seekers", align="center", color="skyblue")
    # ax.legend()

    # plt.savefig('temp_img/'+ self.name +'.png')
    # plt.close(fig)
    

  def __str__(self):
    return "Country: " + self.name + "\nCode: " + self.code + "\nRefugees: \
" + str(self.refugees) + "\nAsylum Seekers: " + str(self.asylum_seekers) + "\nPopulation: \
" + str(self.population) + "\nDistance from South Sudan: " + str(self.distance) + "\nFlag: \
" + self.flag + "\nChart Data: " + str(self.chart)


def main():
  country = Country("", "USA")
  if country.update_country():
    print(country)
  else:
    pass

if __name__ == "__main__":
  main()