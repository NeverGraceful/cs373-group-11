## Canvas / Slack Group Number

Group 11

## Team Members

Sugandha Kumar, Adithya Bhonsley, Benjamin Sandoval, Anna Jimenea, Jean-Claude Bissou

## Project Name

SupportSouthSudan

## Website Link

https://www.supportsouthsudan.me/

## Develop Website Link

https://develop.d1b4k40wgfxdel.amplifyapp.com/

## Git SHA

a4c92ca782812a11c1a4d3987ff9d789a54c3e43

## Project Description

SupportSouthSudan aims to create an understanding of the South Sudanese refugees, including the countries that have accepted them, associated Organizations/organizations, and related news articles. The goal is to increase awareness about the refugees' experiences, highlight the crucial role played by Organizations in assisting their community, and provide up-to-date information on the country's refugee situation.

## Phase Leader

Leader: Benjamin Sandoval
Responsiblities: assuring rubric was followed, overseeing progress, various assistance, visualizations, preping presentation

## Estimated Time Working

Sugandha Kumar: 10 hours
Adithya Bhonsley: 10 hours
Benjamin Sandoval: 20 hours
Anna Jimenea: 10 hours
Jean-Claude Bissou: 12 hours

## Actual Time Working

Sugandha Kumar: 15 hours
Adithya Bhonsley: 15 hours
Benjamin Sandoval: 28 hours
Anna Jimenea: 20 hours
Jean-Claude Bissou: 20 hours

## Postman API

https://documenter.getpostman.com/view/32889511/2s9Yyzde1E

## Data Sources

- UNHCR ODP https://data.unhcr.org/en/situations/southsudan
- ReliefWeb https://reliefweb.int/updates?view=headlines&search=south+sudan+refugee
- UNHCR Data Finder https://www.unhcr.org/refugee-statistics/download/?url=jwA88D
- UNHCR ODP API https://data.unhcr.org/api/doc#/
- REST Countries https://restcountries.com/
- News API https://newsapi.org/
- Youtube API https://developers.google.com/youtube/v3/
- Bing API https://www.microsoft.com/en-us/bing/apis/bing-web-search-api
- Google Maps API https://developers.google.com/maps

## Models

- Countries
- News
- Organizations

## Estimated Number of Instances per Model

- Countries: 250
- News: 306
- Organizations: 135

## Attributes for Each Model

- Countries:

  - Name
  - Pop. of accepted refugees from South Sudan
  - Pop. of asylum-seekers from South Sudan
  - Total Population
  - Distance from South Sudan

- News:
  - Article name
  - Publishing date
  - News Source
  - Country of origin/importance
  - Type of article (press release, analysis, etc.)
  - Link to the article
- Organizations:
  - Name
  - Type of assistance (Food, medical, education, etc.)
  - Location
  - Establishment date
  - Donations/volunteers
  - Amount (monetarily) contributed

## Model Connections

- Countries - News: News articles involving this country
- Countries - Organizations: Countries that the Organizations originate from
- News - Country: What country the news takes place in
- News - Organizations: Organizations that serve as the source of the article
- Organizations - Countries: Countries the Organizations originate from
- Organizations - News: News articles that cover the same topic this organization seeks to help with

## Media for Each Model

- Countries:
  - Map (showing distribution) or graph
  - Text
- News:
  - Image/News Clipping
  - Text
- Organizations:
  - Image (Flyer/Poster)
  - Video
  - Text

## Questions Our Website Will Answer

- What is currently happening in South Sudan as of recently?
- What countries do people who flee from South Sudan seek asylum?
- What resources or organizations can provide support for South Sudanese people once they find asylum?

## Database Diagram

https://dbdiagram.io/d/SupportSouthSudan-6609d15237b7e33fd728a552
