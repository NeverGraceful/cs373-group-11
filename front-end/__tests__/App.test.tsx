import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import App from '../src/App';
import ResizeObserver from 'resize-observer-polyfill';
global.ResizeObserver = ResizeObserver;
import { cancelRequests } from '../src/gitlab_api';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

const mock = new MockAdapter(axios);

// Mock the response for the API requests in your test environment
mock.onGet('https://gitlab.com/api/v4/projects/55997759/repository/commits').reply(200, /* mocked data */);
mock.onGet('https://gitlab.com/api/v4/projects/55997759/issues').reply(200, /* mocked data */);


afterEach(() => {
  // Cancel any ongoing Axios requests after each test
  cancelRequests();
});

// import userEvent from '@testing-library/user-event';
jest.mock('@react-google-maps/api', () => ({
  GoogleMap: jest.fn(() => null), // Mock GoogleMap component
}));

describe('App component', () => {
  beforeEach(() => {
    render(
      <App />
    );
  });

  test('renders without crashing', () => {
    // No need to render App here, as it's already rendered in beforeEach
    // Test logic goes here
  });

  test('renders the navbar', () => {
    const navLinks = screen.getAllByRole('link');
    expect(navLinks).toHaveLength(9);
  });

  test('renders the home page by default', () => {
    const homePageElement = screen.getByText(/Our Purpose/i);
    expect(homePageElement).toBeTruthy();
  });  

  test('renders the about page', () => {
    const aboutLink = screen.getByRole('link', { name: /about/i });
    expect(aboutLink).toBeTruthy();
    fireEvent.click(aboutLink);
    const aboutPageElement = screen.getByText('Meet the Team');
    expect(aboutPageElement).toBeTruthy();
  });

  test('renders the countries page', async () => {
    const countriesLink = screen.getByRole('link', { name: /countries/i });
    expect(countriesLink).toBeTruthy();
    fireEvent.click(countriesLink);
  });

  test('renders the Organizations page', async () => {
    const organizationsLink = screen.getByRole('link', { name: /organizations/i });
    expect(organizationsLink).toBeTruthy();
    fireEvent.click(organizationsLink);
  });

  test('renders the news page', async() => {
    const newsLink = screen.getByRole('link', { name: /news/i });
    expect(newsLink).toBeTruthy();
    fireEvent.click(newsLink);
  }, 15000);

  test('renders the organizations page from news page', async() => {
    const newsLink = screen.getByRole('link', { name: /news/i });
    expect(newsLink).toBeTruthy();
    fireEvent.click(newsLink);
    const organizationsLink = screen.getByRole('link', { name: /organizations/i });
    expect(organizationsLink).toBeTruthy();
    fireEvent.click(organizationsLink);
  }, 15000);

  test('renders the countries page from organizations page', async () => {
    const organizationsLink = screen.getByRole('link', { name: /organizations/i });
    expect(organizationsLink).toBeTruthy();
    fireEvent.click(organizationsLink);
    const countriesLink = screen.getByRole('link', { name: /countries/i });
    expect(countriesLink).toBeTruthy();
    fireEvent.click(countriesLink);
  });
  
  test('renders the about page from countries page', async () => {
    const countriesLink = screen.getByRole('link', { name: /countries/i });
    expect(countriesLink).toBeTruthy();
    fireEvent.click(countriesLink);
    const aboutLink = screen.getByRole('link', { name: /about/i });
    expect(aboutLink).toBeTruthy();
    fireEvent.click(aboutLink);
    const aboutPageElement = screen.getByText('Meet the Team');
    expect(aboutPageElement).toBeTruthy();
  });
  
});
