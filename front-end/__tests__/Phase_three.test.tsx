import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import App from '../src/App';
import ResizeObserver from 'resize-observer-polyfill';
global.ResizeObserver = ResizeObserver;
import { cancelRequests } from '../src/gitlab_api';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

const mock = new MockAdapter(axios);


afterEach(() => {
  // Cancel any ongoing Axios requests after each test
  cancelRequests();
});

// import userEvent from '@testing-library/user-event';
jest.mock('@react-google-maps/api', () => ({
  GoogleMap: jest.fn(() => null), // Mock GoogleMap component
}));

describe('App component', () => {
  beforeEach(() => {
    render(
      <App />
    );
  });

  test('Can search in home page', async () => {
    const searchLink = screen.getByRole('link', { name: /search/i });
    expect(searchLink).toBeTruthy();
    
    const searchInput = screen.getByPlaceholderText(/Search.../i);
    fireEvent.change(searchInput, { target: { value: 'test query' } });

    const searchButton = screen.getByRole('button', { name: /search/i });
    fireEvent.click(searchButton);

    await waitFor(() => {
        const aboutPageElement = screen.getByText('Search Results:');
        expect(aboutPageElement).toBeTruthy();
    });
  });

  test('Can search in country model', async () => {
    const searchLink = screen.getByRole('link', { name: /countries/i });
    expect(searchLink).toBeTruthy();
    
    const searchInput = screen.getByPlaceholderText(/Search.../i);
    fireEvent.change(searchInput, { target: { value: 'test query' } });

    const searchButton = screen.getByRole('button', { name: /search/i });
    fireEvent.click(searchButton);

    await waitFor(() => {
        const aboutPageElement = screen.getByText('Search Results:');
        expect(aboutPageElement).toBeTruthy();
    });
  });

  test('Can search in organizations model', async () => {
    const searchLink = screen.getByRole('link', { name: /organizations/i });
    expect(searchLink).toBeTruthy();
    
    const searchInput = screen.getByPlaceholderText(/Search.../i);
    fireEvent.change(searchInput, { target: { value: 'test query' } });

    const searchButton = screen.getByRole('button', { name: /search/i });
    fireEvent.click(searchButton);

    await waitFor(() => {
        const aboutPageElement = screen.getByText('Search Results:');
        expect(aboutPageElement).toBeTruthy();
    });
  });

  test('Can search in news model', async () => {
    const searchLink = screen.getByRole('link', { name: /news/i });
    expect(searchLink).toBeTruthy();
    
    const searchInput = screen.getByPlaceholderText(/Search.../i);
    fireEvent.change(searchInput, { target: { value: 'test query' } });

    const searchButton = screen.getByRole('button', { name: /search/i });
    fireEvent.click(searchButton);

    await waitFor(() => {
        const aboutPageElement = screen.getByText('Search Results:');
        expect(aboutPageElement).toBeTruthy();
    });
  });

  test('Can pick model for search on home page', async () => {
    const countryButton = screen.getByRole('button', { name: /countries/i });
    fireEvent.click(countryButton);

    const orgButton = screen.getByRole('button', { name: /organizations/i });
    fireEvent.click(orgButton);
    
    const newsButton = screen.getByRole('button', { name: /news/i });
    fireEvent.click(newsButton);
  });

  test('Sort options for country model search', async () => {
    const countriesLinkInNav = screen.getByRole('link', { name: /countries/i });
    fireEvent.click(countriesLinkInNav);

    const sortSelect = screen.getByText(/Sort by/i);
    fireEvent.change(sortSelect, { target: { value: 'Name' } });
    fireEvent.change(sortSelect, { target: { value: 'Number Refugees' } });
    fireEvent.change(sortSelect, { target: { value: 'Number Asylum Seekers' } });
    fireEvent.change(sortSelect, { target: { value: 'Population Accepted' } });
    fireEvent.change(sortSelect, { target: { value: 'Distance from Sudan' } });
  });

  test('Sort options for organizations model search', async () => {
    const orgsLinkInNav = screen.getByRole('link', { name: /organizations/i });
    fireEvent.click(orgsLinkInNav);

    setTimeout(() => {
      const sortSelect = screen.getByText(/Sort by/i);
      fireEvent.change(sortSelect, { target: { value: 'Name' } });
      fireEvent.change(sortSelect, { target: { value: 'Short Name' } });
      fireEvent.change(sortSelect, { target: { value: 'Date Created' } });
    }, 1000);
  });

  test('Sort options for news model search', async () => {
    const newsLinkInNav = screen.getByRole('link', { name: /news/i });
    fireEvent.click(newsLinkInNav);

    setTimeout(() => {
      const sortSelect = screen.getByText(/Sort by/i);
      fireEvent.change(sortSelect, { target: { value: 'Name' } });
      fireEvent.change(sortSelect, { target: { value: 'Date' } });
    }, 1000);
  });


  test('Filter options for organizations model search', async () => {
    const orgsLinkInNav = screen.getByRole('link', { name: /organizations/i });
    fireEvent.click(orgsLinkInNav);


    setTimeout(() => {
      const filter1Select = screen.getByText(/Org Type/i);
      fireEvent.change(filter1Select, { target: { value: 'Other' } });
      fireEvent.change(filter1Select, { target: { value: 'Government' } });
      fireEvent.change(filter1Select, { target: { value: 'Media' } });
    }, 1000);

    setTimeout(() => {
      const filter2Select = screen.getByText(/Status/i);
      fireEvent.change(filter2Select, { target: { value: 'Active' } });
      fireEvent.change(filter2Select, { target: { value: 'Inactive' } });
    }, 1000);
  });

  test('Filter options for news model search', async () => {
    const newsLinkInNav = screen.getByRole('link', { name: /news/i });
    fireEvent.click(newsLinkInNav);
    
    setTimeout(() => {
      const filter1Select = screen.getByText(/News Type/i);
      fireEvent.change(filter1Select, { target: { value: 'Other' } });
      fireEvent.change(filter1Select, { target: { value: 'Map' } });
      fireEvent.change(filter1Select, { target: { value: 'Assessment' } });
    }, 1000);

    setTimeout(() => {
      const filter2Select = screen.getByText(/News Type/i);
      fireEvent.change(filter2Select, { target: { value: 'HRW' } });
      fireEvent.change(filter2Select, { target: { value: 'NRC' } });
      fireEvent.change(filter2Select, { target: { value: 'GPEI' } });
    }, 1000);

    setTimeout(() => {
      const filter3Select = screen.getByText(/News Type/i);
      fireEvent.change(filter3Select, { target: { value: 'Haiti' } });
      fireEvent.change(filter3Select, { target: { value: 'Serbia' } });
      fireEvent.change(filter3Select, { target: { value: 'Dominica' } });
    }, 1000);
  });
});