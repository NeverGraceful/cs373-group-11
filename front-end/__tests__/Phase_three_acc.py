from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
import time


# Initialize WebDriver (assuming Chrome for this example)
driver = webdriver.Chrome()

# Function to wait for element visibility
def wait_for_element(selector):
    return WebDriverWait(driver, 30).until(EC.visibility_of_element_located(selector))

# Test: renders the navbar
# def test_renders_the_navbar():
#     driver.get("https://develop.d1b4k40wgfxdel.amplifyapp.com/home")
#     navbar_element = wait_for_element("[role='navigation']")
#     assert navbar_element is not None

# def test_renders_the_navbar():
#     driver.get("https://develop.d1b4k40wgfxdel.amplifyapp.com/home")
#     # Wait for the navigation element to be present
#     navbar_element = wait_for_element("[class='nav-link']")
#     assert navbar_element is not None
#     print("test_renders_the_navbar passed.")

def test_renders_the_navbar():
    driver.get("https://develop.d1b4k40wgfxdel.amplifyapp.com/home")
    # Wait for the navigation element to be present
    navbar_element = wait_for_element((By.CLASS_NAME, "nav-link"))
    assert navbar_element is not None
    print("test_renders_the_navbar passed.")

# Test: renders the home page by default
def test_renders_the_home_page_by_default():
    driver.get("https://develop.d1b4k40wgfxdel.amplifyapp.com/home")
    home_page_element = wait_for_element((By.XPATH, "//*[contains(text(),'Our Purpose')]"))
    assert home_page_element is not None
    print("test_renders_the_home_page_by_default passed.")

# Test: renders the about page
def test_renders_the_about_page():
    driver.get("https://develop.d1b4k40wgfxdel.amplifyapp.com/home")
    about_link = wait_for_element((By.CSS_SELECTOR, "[class='nav-link'][href='/about']"))
    about_link.click()
    about_page_element = wait_for_element((By.XPATH, "//*[contains(text(),'Meet the Team')]"))
    assert about_page_element is not None
    print("test_renders_the_about_page passed.")

# Test: renders the countries page
def test_renders_the_countries_page():
    driver.get("https://develop.d1b4k40wgfxdel.amplifyapp.com/home")
    countries_link = wait_for_element((By.CSS_SELECTOR, "[class='nav-link'][href='/countries']"))
    countries_link.click()
    page_title = wait_for_element((By.XPATH, "//*[contains(@class,'title')][contains(text(),'Countries')]"))
    assert page_title is not None
    print("test_renders_the_countries_page passed.")

# Test: renders the Organizations page
def test_renders_the_organizations_page():
    driver.get("https://develop.d1b4k40wgfxdel.amplifyapp.com/home")
    organizations_link = wait_for_element((By.CSS_SELECTOR, "[class='nav-link'][href='/orgs']"))
    organizations_link.click()
    page_title = wait_for_element((By.XPATH, "//*[contains(@class,'title')][contains(text(),'Organizations')]"))
    assert page_title is not None
    print("test_renders_the_organizations_page passed.")

# Test: renders the news page
def test_renders_the_news_page():
    driver.get("https://develop.d1b4k40wgfxdel.amplifyapp.com/home")
    news_link = wait_for_element((By.CSS_SELECTOR, "[class='nav-link'][href='/news']"))
    news_link.click()
    page_title = wait_for_element((By.XPATH, "//*[contains(@class,'title')][contains(text(),'News')]"))
    assert page_title is not None
    print("test_renders_the_news_page passed.")

def test_countries_sorting():
    driver.get("https://develop.d1b4k40wgfxdel.amplifyapp.com/home")
    news_link = wait_for_element((By.CSS_SELECTOR, "[class='nav-link'][href='/countries']"))
    news_link.click()

    sorting_dropdown = Select(driver.find_element(By.CSS_SELECTOR, "select.form-select"))
    sorting_dropdown.select_by_value('name')
    time.sleep(5)
    print("test_countries_sorting passed.")

def test_organizations_sorting():
    driver.get("https://develop.d1b4k40wgfxdel.amplifyapp.com/home")
    news_link = wait_for_element((By.CSS_SELECTOR, "[class='nav-link'][href='/orgs']"))
    news_link.click()

    sorting_dropdown = wait_for_element((By.CSS_SELECTOR, "select.form-select"))
    sorting_options = [option.get_attribute("value") for option in sorting_dropdown.find_elements(By.TAG_NAME, "option")]
    if sorting_options:
        sorting_dropdown = Select(sorting_dropdown)
        sorting_dropdown.select_by_value(sorting_options[0])
    else:
        print("No sorting options found.")
    print("test_countries_sorting passed.")

def test_news_sorting():
    driver.get("https://develop.d1b4k40wgfxdel.amplifyapp.com/home")
    news_link = wait_for_element((By.CSS_SELECTOR, "[class='nav-link'][href='/news']"))
    news_link.click()

    sorting_dropdown = wait_for_element((By.CSS_SELECTOR, "select.form-select"))
    sorting_options = [option.get_attribute("value") for option in sorting_dropdown.find_elements(By.TAG_NAME, "option")]
    if sorting_options:
        sorting_dropdown = Select(sorting_dropdown)
        sorting_dropdown.select_by_value(sorting_options[0])
    else:
        print("No sorting options found.")
    print("test_news_sorting passed.")

def test_home_search():
    driver.get("https://develop.d1b4k40wgfxdel.amplifyapp.com/home")
    search_bar = wait_for_element((By.CSS_SELECTOR, "input.form-control"))
    assert search_bar is not None, "Search bar is not present on the page"
    print("test_home_search passed.")

def test_country_search():
    driver.get("https://develop.d1b4k40wgfxdel.amplifyapp.com/home")
    news_link = wait_for_element((By.CSS_SELECTOR, "[class='nav-link'][href='/countries']"))
    news_link.click()

    search_bar = wait_for_element((By.CSS_SELECTOR, "input.form-control"))
    assert search_bar is not None, "Search bar is not present on the page"
    dropdowns = driver.find_elements(By.CSS_SELECTOR, "select.form-select")
    assert dropdowns, "No dropdowns found on the page"

    print("test_country_search passed.")

def test_organizations_search():
    driver.get("https://develop.d1b4k40wgfxdel.amplifyapp.com/home")
    news_link = wait_for_element((By.CSS_SELECTOR, "[class='nav-link'][href='/orgs']"))
    news_link.click()

    search_bar = wait_for_element((By.CSS_SELECTOR, "input.form-control"))
    assert search_bar is not None, "Search bar is not present on the page"
    dropdowns = driver.find_elements(By.CSS_SELECTOR, "select.form-select")
    assert dropdowns, "No dropdowns found on the page"

    print("test_organizations_search passed.")

def test_news_search():
    driver.get("https://develop.d1b4k40wgfxdel.amplifyapp.com/home")
    news_link = wait_for_element((By.CSS_SELECTOR, "[class='nav-link'][href='/news']"))
    news_link.click()

    search_bar = wait_for_element((By.CSS_SELECTOR, "input.form-control"))
    assert search_bar is not None, "Search bar is not present on the page"
    dropdowns = driver.find_elements(By.CSS_SELECTOR, "select.form-select")
    assert dropdowns, "No dropdowns found on the page"

    print("test_news_search passed.")


test_renders_the_navbar()
test_renders_the_home_page_by_default()
test_renders_the_about_page()
test_renders_the_countries_page()
test_renders_the_organizations_page()
test_renders_the_news_page()
test_countries_sorting()
test_organizations_sorting()
test_news_sorting()
test_home_search()
test_country_search()
test_organizations_search()
test_news_search()

# Close the WebDriver session
driver.quit()