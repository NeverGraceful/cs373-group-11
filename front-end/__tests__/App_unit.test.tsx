import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import App from '../src/App';
import ResizeObserver from 'resize-observer-polyfill';
global.ResizeObserver = ResizeObserver;
import { cancelRequests } from '../src/gitlab_api';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
// import '@testing-library/jest-dom';

const mock = new MockAdapter(axios);

// Mock the response for the API requests in your test environment
mock.onGet('https://gitlab.com/api/v4/projects/55997759/repository/commits').reply(200, /* mocked data */);
mock.onGet('https://gitlab.com/api/v4/projects/55997759/issues').reply(200, /* mocked data */);

afterEach(() => {
  // Cancel any ongoing Axios requests after each test
  cancelRequests();
});

jest.mock('@react-google-maps/api', () => ({
  GoogleMap: jest.fn(() => null), // Mock GoogleMap component
}));

require('canvas');

describe('App component', () => {

  beforeEach(() => {
      render(
        <App />
      );
  });

  test('renders navigation links correctly', () => {
    const navLinks = screen.getAllByRole('link');
    expect(navLinks).toHaveLength(9);
  });

  test('navigates to home page by default', () => {
    const homePageElement = screen.getByText(/Our Purpose/i);
    expect(homePageElement).toBeTruthy();
  }); 

  test('navigates to any page regardless of where it is', async () => {
    const homePageElement = screen.getByText(/Our Purpose/i);
    expect(homePageElement).toBeTruthy();

    const aboutLink = screen.getByRole('link', { name: /about/i });
    expect(aboutLink).toBeTruthy();
    fireEvent.click(aboutLink);
    const aboutPageElement = screen.getByText('Meet the Team');
    expect(aboutPageElement).toBeTruthy();

    const countriesLink = screen.getByRole('link', { name: /countries/i });
    expect(countriesLink).toBeTruthy();
    fireEvent.click(countriesLink);

    const organizationsLinkInNav = screen.getByRole('link', { name: /organizations/i });
    expect(organizationsLinkInNav).toBeTruthy();
    fireEvent.click(organizationsLinkInNav);

    const newsLinkInNav = screen.getByRole('link', { name: /news/i });
    expect(newsLinkInNav).toBeTruthy();
    fireEvent.click(newsLinkInNav);
  });

  test('navigates to any page from about page', async () => {
    const aboutLinkInNav = screen.getByRole('link', { name: /about/i });
    expect(aboutLinkInNav).toBeTruthy();
    fireEvent.click(aboutLinkInNav);
    const aboutPageElement = screen.getByText('Meet the Team');
    expect(aboutPageElement).toBeTruthy();
    
    const countriesLinkInNav = screen.getByRole('link', { name: /countries/i });
    expect(countriesLinkInNav).toBeTruthy();
    fireEvent.click(countriesLinkInNav);

    fireEvent.click(aboutLinkInNav);
    const organizationsLinkInNav = screen.getByRole('link', { name: /organizations/i });
    expect(organizationsLinkInNav).toBeTruthy();
    fireEvent.click(organizationsLinkInNav);

    fireEvent.click(aboutLinkInNav);
    const newsLinkInNav = screen.getByRole('link', { name: /news/i });
    expect(newsLinkInNav).toBeTruthy();
    fireEvent.click(newsLinkInNav);
  });

  test('navigates to any page from countries page', async () => {
    const countriesLinkInNav = screen.getByRole('link', { name: /countries/i });
    expect(countriesLinkInNav).toBeTruthy();
    fireEvent.click(countriesLinkInNav);

    const aboutLinkInNav = screen.getByRole('link', { name: /about/i });
    expect(aboutLinkInNav).toBeTruthy();
    fireEvent.click(aboutLinkInNav);

    fireEvent.click(countriesLinkInNav);
    const organizationsLinkInNav = screen.getByRole('link', { name: /organizations/i });
    expect(organizationsLinkInNav).toBeTruthy();
    fireEvent.click(organizationsLinkInNav);

    fireEvent.click(countriesLinkInNav);
    const newsLinkInNav = screen.getByRole('link', { name: /news/i });
    expect(newsLinkInNav).toBeTruthy();
    fireEvent.click(newsLinkInNav);
  });

  test('navigates to any page from organizations page', async () => {
    const organizationsLinkInNav = screen.getByRole('link', { name: /organizations/i });
    expect(organizationsLinkInNav).toBeTruthy();
    fireEvent.click(organizationsLinkInNav);

    const countriesLinkInNav = screen.getByRole('link', { name: /countries/i });
    expect(countriesLinkInNav).toBeTruthy();
    fireEvent.click(countriesLinkInNav);

    fireEvent.click(organizationsLinkInNav);
    const aboutLinkInNav = screen.getByRole('link', { name: /about/i });
    expect(aboutLinkInNav).toBeTruthy();
    fireEvent.click(aboutLinkInNav);

    fireEvent.click(organizationsLinkInNav);
    const newsLinkInNav = screen.getByRole('link', { name: /news/i });
    expect(newsLinkInNav).toBeTruthy();
    fireEvent.click(newsLinkInNav);
  });

  test('navigates to any page from news page', async () => {
    const newsLinkInNav = screen.getByRole('link', { name: /news/i });
    expect(newsLinkInNav).toBeTruthy();
    fireEvent.click(newsLinkInNav);

    const organizationsLinkInNav = screen.getByRole('link', { name: /organizations/i });
    expect(organizationsLinkInNav).toBeTruthy();
    fireEvent.click(organizationsLinkInNav);

    fireEvent.click(newsLinkInNav);
    const countriesLinkInNav = screen.getByRole('link', { name: /countries/i });
    expect(countriesLinkInNav).toBeTruthy();
    fireEvent.click(countriesLinkInNav);

    fireEvent.click(newsLinkInNav);
    const aboutLinkInNav = screen.getByRole('link', { name: /about/i });
    expect(aboutLinkInNav).toBeTruthy();
    fireEvent.click(aboutLinkInNav);
  });

  test('navigates to any page from home page', async () => {
    const homeLinkInNav = screen.getByRole('link', { name: /supportsouthsudan/i });
    expect(homeLinkInNav).toBeTruthy();
    fireEvent.click(homeLinkInNav);

    const newsLinkInNav = screen.getByRole('link', { name: /news/i });
    expect(newsLinkInNav).toBeTruthy();
    fireEvent.click(newsLinkInNav);

    fireEvent.click(homeLinkInNav);
    const organizationsLinkInNav = screen.getByRole('link', { name: /organizations/i });
    expect(organizationsLinkInNav).toBeTruthy();
    fireEvent.click(organizationsLinkInNav);

    fireEvent.click(homeLinkInNav);
    const countriesLinkInNav = screen.getByRole('link', { name: /countries/i });
    expect(countriesLinkInNav).toBeTruthy();
    fireEvent.click(countriesLinkInNav);

    fireEvent.click(homeLinkInNav);
    const aboutLinkInNav = screen.getByRole('link', { name: /about/i });
    expect(aboutLinkInNav).toBeTruthy();
    fireEvent.click(aboutLinkInNav);
  });

  test('navigates to home page from any page', async () => {
    const homeLinkInNav = screen.getByRole('link', { name: /supportsouthsudan/i });
    expect(homeLinkInNav).toBeTruthy();
    fireEvent.click(homeLinkInNav);

    const newsLinkInNav = screen.getByRole('link', { name: /news/i });
    expect(newsLinkInNav).toBeTruthy();
    fireEvent.click(newsLinkInNav);

    const organizationsLinkInNav = screen.getByRole('link', { name: /organizations/i });
    expect(organizationsLinkInNav).toBeTruthy();
    fireEvent.click(organizationsLinkInNav);

    fireEvent.click(homeLinkInNav);
    const countriesLinkInNav = screen.getByRole('link', { name: /countries/i });
    expect(countriesLinkInNav).toBeTruthy();
    fireEvent.click(countriesLinkInNav);

    const aboutLinkInNav = screen.getByRole('link', { name: /about/i });
    expect(aboutLinkInNav).toBeTruthy();
    fireEvent.click(aboutLinkInNav);
    fireEvent.click(homeLinkInNav);
  });

  test('renders tools with correct attributes and text', async () => {
    const aboutLinkInNav = screen.getByRole('link', { name: /about/i });
    expect(aboutLinkInNav).toBeTruthy();
    fireEvent.click(aboutLinkInNav);
    
    const toolLinks = screen.getAllByRole('link', { name: /Tool Logo/i });
    expect(toolLinks).toBeTruthy();
    const expectedCount = 5;
    expect(toolLinks.length).toBeGreaterThanOrEqual(expectedCount);
  });
});
