import React from "react";
import "./App.css";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import AboutPage from "./components/About/AboutPage";
import CharitiesPage from "./components/Orgs/CharitiesPage";
import CountriesPage from "./components/Countries/CountriesPage";
import HomePage from "./components/Home_and_Search/HomePage";
import NewsPage from "./components/News/NewsPage";
import NavBar from "./components/Header_and_Footer/NavBar";
import SearchPage from "./components/Home_and_Search/SearchPage";
import Visualizations from "./components/Visuals/Visualizations";
import DevVisualizations from "./components/Visuals/DevVisualizations";
import CountryInstance from "./components/Countries/CountryInstance";
import NewsInstance from "./components/News/NewsInstance";
import CharityInstance from "./components/Orgs/CharityInstance";

function App() {
  return (
    <BrowserRouter>
      <NavBar />
      <Routes>
        <Route path="/" element={<Navigate replace to="/home" />} />
        <Route path="/home" element={<HomePage />} />
        <Route path="/about" element={<AboutPage />} />
        <Route path="/countries" element={<CountriesPage />} />
        <Route path="/orgs" element={<CharitiesPage />} />
        <Route path="/news" element={<NewsPage />} />
        <Route path="/countries/:name" element={<CountryInstance />} />
        <Route path="/news/:id" element={<NewsInstance />} />
        <Route path="/orgs/:name" element={<CharityInstance />} />
        <Route path="/search" element={<SearchPage />} />
        <Route path="/visualizations" element={<Visualizations />} />
        <Route path="/devvisualizations" element={<DevVisualizations />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
