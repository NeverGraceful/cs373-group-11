import React from "react";
import { useEffect, useState } from "react";
import YouTube from "react-youtube";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import "../Instance_Generics/InstancePage.css";
import InstanceCard from "../Instance_Generics/InstanceCard";
import { useLocation } from "react-router-dom";
import axios from "axios";

export interface InstanceData {
  name: string;
  image: string;
  date_created: string;
  country: [string];
  type: string;
  description: string;
  homepage: string;
  status: string;
  news_id: [number];
  news: [string];
}

const CharityInstance = () => {
  const location = useLocation();
  const [youtubeID, setYoutubeID] = useState("");

  useEffect(() => {
    const params = {
      part: "snippet",
      maxResults: 1,
      q: location.state.data.name,
      key: "AIzaSyCnYrLsp-00ZiNuxVvM8oNjTNhCSLtsDjI",
      type: "video",
    };
    axios
      .get("https://www.googleapis.com/youtube/v3/search", { params })
      .then((response) => {
        const items = response.data.items;
        if (items && items.length > 0) {
          const video_id = items[0].id.videoId;
          setYoutubeID(video_id);
        }
      })
      .catch((error) => {
        console.log("Error", error);
      });
  });

  var split_ids = "";
  if (location.state.data.news_id.length > 0) {
    split_ids = location.state.data.news_id[0].split(",");
  }

  return (
    <div className="container mt-4">
      <div className="row align-items-center">
        <div className="col-md-3">
          {/* Logo image */}
          <img
            src={location.state.data.image}
            alt="Logo"
            className="img-fluid"
          />
        </div>
        <div className="col-md-9">
          {/* Title */}
          <h1 className="title mb-4">{location.state.data.name}</h1>
        </div>
      </div>
      <div className="row">
        <div className="col-md-7">
          <div className="description-box mb-4">
            <YouTube videoId={youtubeID} />
            <br />
            <p>{location.state.data.description}</p>
          </div>
        </div>

        <div className="col-md-5">
          <div className="data-box">
            <div className="data-row">
              <span className="data-label"> Status: </span>
              <span className="data-value">{location.state.data.status}</span>
            </div>
            <div className="data-row">
              <span className="data-label"> Headquarters Location: </span>
              <span className="data-value">{location.state.data.country}</span>
            </div>
            <div className="data-row">
              <span className="data-label"> Date of Establishment: </span>
              <span className="data-value">
                {location.state.data.date_created}
              </span>
            </div>
            <div className="data-row">
              <span className="data-label"> Type of organization: </span>
              <span className="data-value">{location.state.data.type}</span>
            </div>
            <br />
            <div className="data-row">
              <button
                onClick={() => window.open(location.state.data.Link, "_blank")}
                className="btn btn-dark"
              >
                Homepage
              </button>
            </div>
            <br />
          </div>
        </div>
        <div className="row">
          <div className="row-md-6">
            <div className="data-row">
              <span className="data-label">News: </span>
              <Row>
                {location.state.data.news &&
                  location.state.data.news
                    .slice(0, 5)
                    .map((news: string, index: number) => {
                      const trimmed = news.trim();
                      if (trimmed) {
                        return (
                          <Col key={index} xs={6} md={4} lg={4}>
                            <InstanceCard
                              instanceCardData={{
                                name: trimmed,
                                link: `../../news/${trimmed}`,
                                inst_type: "news",
                                news_id: parseInt(split_ids[index], 10),
                              }}
                            />
                          </Col>
                        );
                      }
                      return null;
                    })}
                {location.state.data.news.length === 0 && (
                  <Col>
                    <h1 style={{ textAlign: "center" }}>
                      Connection not found
                    </h1>
                    ;
                  </Col>
                )}
              </Row>
            </div>
          </div>
          <div className="row-md-6">
            <div className="data-row">
              <span className="data-label">Countries: </span>
              <Row>
                {location.state.data.country &&
                  location.state.data.country
                    .slice(0, 5)
                    .map((country: string, index: number) => {
                      const trimmed = country.trim();
                      if (trimmed) {
                        return (
                          <Col key={index} xs={6} md={4} lg={3}>
                            <InstanceCard
                              instanceCardData={{
                                name: trimmed,
                                link: `../../countries/${trimmed}`,
                                inst_type: "countries",
                                news_id: -1,
                              }}
                            />
                          </Col>
                        );
                      }
                      return null;
                    })}
                {location.state.data.country.length === 0 && (
                  <Col>
                    <h1 style={{ textAlign: "center" }}>
                      Connection not found
                    </h1>
                    ;
                  </Col>
                )}
              </Row>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CharityInstance;
