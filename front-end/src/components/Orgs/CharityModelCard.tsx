import React from "react";
import { useNavigate } from "react-router-dom";

interface CardProps {
  name: string;
  image: string;
  date_created: string;
  country: [string];
  type: string;
  description: string;
  homepage: string;
  status: string;
  news_id: [number];
  news: [string];
}

interface CardComponentProps {
  data: CardProps;
  searchQuery: string;
}

const CharityModelCard: React.FC<CardComponentProps> = ({ data, searchQuery }) => {
  const navigate = useNavigate();

  const highlightSearchQuery = (text: string, query: string) => {
    if (!text || !query.trim()) return text;
    const regex = new RegExp(`(${query})`, 'gi');
    return text.replace(regex, '<mark>$1</mark>');
  };

  return (
    <div className="col-md-4 mb-4" data-testid="charity-model-card">
      <div
        className="card p-3 border rounded-3 shadow"
        onClick={() => navigate(`/orgs/${data.name}`, { state: { data: data } })}
        style={{ width: "300px", height: "500px", backgroundColor: "#e8ddd8" }}
      >
        <img
          src={data.image}
          className="card-img-top"
          style={{ height: "200px", objectFit: "cover" }}
          alt={data.image}
        />
        <div
          className="card-body"
          style={{ maxHeight: "600px", overflowY: "auto" }}
        >
          {/* Highlight search query in card title */}
          <h5 className="card-title" dangerouslySetInnerHTML={{ __html: highlightSearchQuery(data.name, searchQuery) }} />

          {/* Highlight search query in other card attributes */}
          <p className="card-text">Type of organization: <span dangerouslySetInnerHTML={{ __html: highlightSearchQuery(data.type, searchQuery) }} /></p>
          <p className="card-text">Headquarters: <span dangerouslySetInnerHTML={{ __html: highlightSearchQuery(data.country[0], searchQuery) }} /></p>
          <p className="card-text">Established: <span dangerouslySetInnerHTML={{ __html: highlightSearchQuery(data.date_created, searchQuery) }} /></p>
          <p className="card-text">Status: <span dangerouslySetInnerHTML={{ __html: highlightSearchQuery(data.status, searchQuery) }} /></p>
        </div>
      </div>
    </div>
  );
};

export default CharityModelCard;
