import React from "react";
import { useState, useEffect } from "react";
import CharityModelCard from "./CharityModelCard";
import Footer from "../Header_and_Footer/Footer";

import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";

import axios from "axios";
import master_list from "../News/FilterRef";

interface CharitiesInstance {
  name: string;
  image: string;
  date_created: string;
  country: [string];
  type: string;
  description: string;
  homepage: string;
  status: string;
  news_id: [number];
  news: [string];
}

function CharitiesPage() {
  const numCardsPerPage = 8;
  const [totalInstances, setTotalInstances] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [loaded, setLoaded] = useState(false);
  const [totalPages, setTotalPages] = useState(0);
  const [charitiesInstances, setCharitiesInstances] = useState<
    CharitiesInstance[]
  >([]);

  const [searchQuery, setSearchQuery] = useState("");
  const [sortBy, setSortBy] = useState("");
  const [orgType, setOrgType] = useState("");
  const [orgStatus, setOrgStatus] = useState("");
  const [payload, fillPayload] = useState("")

  const handleSearch = async (event: React.FormEvent) => {
    event.preventDefault();

    fillPayload(`&search=${encodeURIComponent(searchQuery)}&sort=${sortBy}&type=${orgType}&status=${orgStatus}`)
    setCurrentPage(1);
  };

  const nextPage = () => {
    setCurrentPage((prevPage) => Math.min(prevPage + 1, totalPages));
  };

  const prevPage = () => {
    setCurrentPage((prevPage) => Math.max(prevPage - 1, 1));
  };

  const goToPage = (pageNumber: number) => {
    setCurrentPage(pageNumber);
  };

  useEffect(() => {
    axios
      .get(
        `https://api.supportsouthsudan.me/orgs?page=${currentPage}&limit=${numCardsPerPage}${payload}`
      )
      .then((response) => {
        setCharitiesInstances(response.data.orgs);
        setTotalInstances(response.data._metadata.total)
        setTotalPages(Math.ceil(totalInstances / numCardsPerPage))
        setLoaded(true);
      })
      .catch((error) => {
        console.log("Error", error);
        setLoaded(true);
      });
  }, [currentPage, payload, totalInstances]);

  if (!loaded) {
    return <h1 style={{ textAlign: "center", color: "#e8ddd8" }}>Page Loading...</h1>;
  }

  return (
    <div>
      <div className="container mt-5" style={{ color: "#e8ddd8" }}>
        <h1 className="title text-center mb-4" style={{ color: "#FF4928" }}>
          Organizations
        </h1>
        <p>
          A catalog of various charities and organizations actively involved in
          providing assistance and support to South Sudanese refugees. It
          includes information about their mission, types of assistance offered,
          whether they operate as volunteer or donation-based enterprises, as
          well as details on their location and year of establishment. This
          model showcases the diverse range of efforts undertaken by charitable
          organizations to address the needs of refugees. It serves as a
          resource for individuals and groups seeking opportunities to
          contribute, volunteer, or collaborate with organizations dedicated to
          refugee assistance.
        </p>
        <form
        onSubmit={handleSearch}
        className="search-form"
        style={{ margin: "20px auto", maxWidth: "1100px" }}
        >
          <div className="input-group mb-3">
            <input
              type="text"
              className="form-control"
              placeholder="Search..."
              value={searchQuery}
              onChange={(e) => setSearchQuery(e.target.value)}
              style={{ flex: "4" }}
            />
            <select
              className="form-select"
              value={orgType}
              onChange={(e) => setOrgType(e.target.value)}
              style={{ flex: "1" }}
            >
              <option value="">Org Type</option>
              {master_list[3].map((type) => (
                <option key={type} value={`${type}`}>{type}</option>
              ))}
            </select>
            <select
              className="form-select"
              value={orgStatus}
              onChange={(e) => setOrgStatus(e.target.value)}
              style={{ flex: "1" }}
            >
              <option value="">Status</option>
              <option value="Active">Active</option>
              <option value="Inactive">Inactive</option>
            </select>
            <select
              className="form-select"
              value={sortBy}
              onChange={(e) => setSortBy(e.target.value)}
              style={{ flex: "1" }}
            >
              <option value="">Sort by</option>
              <option value="name">Name</option>
              <option value="short_name">Short Name</option>
              <option value="date_created">Date Created</option>
            </select>
            <button
              type="submit"
              className="btn btn-secondary"
              style={{
                backgroundColor: "#60755a",
                borderColor: "#60755a",
                color: "white",
              }}
            >
              Search
            </button>
          </div>
        </form>
        <p data-testid="instance-counter">
          - Instances in this model: {totalInstances}
        </p>
        <p>- Number of pages {totalPages}</p>
        <p>- Current page: {currentPage}</p>
        <br />
        <Row className="justify-content-center">
          {charitiesInstances.map((instance) => {
            return (
              <Col className="mb-3" key={instance.name}>
                <CharityModelCard data={instance} searchQuery={searchQuery}/>
              </Col>
            );
          })}
        </Row>
        <div style={{ textAlign: "center", marginBottom: "20px" }}>
          <Button
            variant="dark"
            onClick={prevPage}
            disabled={currentPage === 1}
            style={{ marginRight: "10px" }}
          >
            Prev
          </Button>
          {Array.from({ length: totalPages }, (_, i) => i + 1).map(
            (pageNumber) => (
              <Button
                key={pageNumber}
                variant={currentPage === pageNumber ? "light" : "light"}
                style={
                  currentPage === pageNumber
                    ? { backgroundColor: "lightgray", marginRight: "10px" }
                    : { backgroundColor: "white", marginRight: "10px" }
                }
                onClick={() => goToPage(pageNumber)}
              >
                {pageNumber}
              </Button>
            )
          )}
          <Button
            variant="dark"
            onClick={nextPage}
            disabled={currentPage === totalPages}
            style={{ marginLeft: "10px" }}
          >
            Next
          </Button>
        </div>
      </div>
      <div>
        <Footer />
      </div>
    </div>
  );
}

export default CharitiesPage;