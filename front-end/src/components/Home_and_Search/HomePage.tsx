import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import "./HomePage.css";
import YouTube from "react-youtube";
import Carousel from "react-bootstrap/Carousel";
import Footer from "../Header_and_Footer/Footer";

function HomePage() {
  const navigate = useNavigate();
  let carouselSlides = [
    {
      img: require("../../media/GettyImages-461217915.jpg"),
      alt: "South Sudanese families accepted as refugees in Uganda shelter in the shade of a tree at the Ochaya Rhino refugee camp in Arua District.",
    },
    {
      img: require("../../media/106641_south_sudan_refugees.jpg"),
      alt: "South Sudanese refugees arriving at Imvepi Refugee Settlement.",
    },
    {
      img: require("../../media/UK395-0004-028-850x566.jpg"),
      alt: "Refugees entering Uganda from South Sudan.",
    },
    {
      img: require("../../media/SouthSudan_Uganda_UNHCR_RF2.jpg"),
      alt: "South Sudanese refugees queue for food at a reception centre in Arua, in northern Uganda.",
    },
    {
      img: require("../../media/image1170x530cropped.jpg"),
      alt: "Hundreds of newly arrived Sudanese refugees gather to receive UNHCR relief kits at the Madjigilta site in Chad's Ouaddaï region.",
    },
  ];

  const [searchQuery, setSearchQuery] = useState("");

  const handleSearch = (event: any) => {
    event.preventDefault();
    navigate(`/search`, { state: { data: {search: searchQuery}} });
  };

  const [index, setIndex] = useState(0);
  const handleSelect = (selectedIndex: any) => {
    setIndex(selectedIndex);
  };

  return (
    <div className="homepage-container" style={{ color: "#FF4928" }}>
      <div
        className="description-container"
        style={{
          marginTop: "50px",
          padding: "20px",
          textAlign: "center",
          width: "1000px",
          margin: "auto",
        }}
      >
        <h2 className="title">Our Purpose</h2>
        <p style={{ marginTop: "20px", color: "#d6c7bf" }}>
          SupportSouthSudan aims to create an understanding of the South
          Sudanese refugees, including the countries that have accepted them,
          associated charities/organizations, and related news articles. The
          goal is to increase awareness about the refugees' experiences,
          highlight the crucial role played by charities in assisting their
          community, and provide up-to-date information on the country's refugee
          situation.
        </p>
      </div>
      <form
        onSubmit={handleSearch}
        className="search-form"
        style={{ margin: "20px auto", maxWidth: "1100px" }}
        >
          <div className="input-group mb-3">
            <input
              type="text"
              className="form-control"
              placeholder="Search..."
              value={searchQuery}
              onChange={(e) => setSearchQuery(e.target.value)}
              style={{ flex: "4" }}
            />
            <button
              type="submit"
              className="btn btn-secondary"
              style={{
                backgroundColor: "#60755a",
                borderColor: "#60755a",
                color: "white",
              }}
            >
              Search
            </button>
          </div>
        </form>
      <div
        className="carousel-container"
        style={{
          position: "relative",
          borderRadius: "10px",
          overflow: "hidden",
        }}
      >
        <Carousel
          activeIndex={index}
          onSelect={handleSelect}
          style={{ borderRadius: "10px", width: "80%", margin: "auto" }}
        >
          {carouselSlides.map((slide, idx) => (
            <Carousel.Item key={idx}>
              <div
                style={{
                  height: "60vh",
                  overflow: "hidden",
                  borderRadius: "10px",
                }}
              >
                <img
                  src={slide.img}
                  alt={slide.alt}
                  className="d-block w-100"
                  style={{ height: "100%", borderRadius: "10px" }}
                />
              </div>
              <div className="dark-overlay"></div> {/* Dark overlay */}
            </Carousel.Item>
          ))}
        </Carousel>
        <Link to="/orgs">
          <button
            type="button"
            className="btn btn-dark"
            style={{
              position: "absolute",
              top: "50%",
              left: "50%",
              transform: "translate(-50%, -50%)",
              zIndex: 1,
              width: "200px",
              height: "60px",
              backgroundColor: "#60755a",
              borderColor: "#60755a",
              color: "white",
            }}
          >
            Donate
          </button>
        </Link>
      </div>
      <div
        className="explore-container"
        style={{ textAlign: "center", marginTop: "75px" }}
      >
        <h2>Explore</h2>
        <div
          style={{ margin: "0 auto", marginBottom: "20px", marginTop: "40px" }}
        >
          <button
            type="button"
            className="btn"
            style={{
              marginRight: "100px",
              width: "150px",
              backgroundColor: "#60755a",
              borderColor: "#60755a",
              color: "white",
            }}
            onClick={() => navigate(`/countries`)}
          >
            Countries
          </button>
          <button
            type="button"
            className="btn"
            style={{
              marginRight: "100px",
              width: "150px",
              backgroundColor: "#60755a",
              borderColor: "#60755a",
              color: "white",
            }}
            onClick={() => navigate(`/orgs`)}
          >
            Organizations
          </button>
          <button
            type="button"
            className="btn"
            style={{
              width: "150px",
              backgroundColor: "#60755a",
              borderColor: "#60755a",
              color: "white",
            }}
            onClick={() => navigate(`/news`)}
          >
            News
          </button>
        </div>
      </div>
      <div className="text-center" style={{ marginTop: "100px" }}>
        <h2>More Information</h2>
      </div>
      <div
        style={{
          height: "1000px",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          marginTop: "-275px",
        }}
      >
        <div style={{ marginRight: "20px" }}>
          <YouTube videoId="2-pVh8rj8Qw" />
        </div>
        <div>
          <YouTube videoId="VKaKXOICS3Q" />
        </div>
      </div>
      <div
        style={{
          margin: "0 auto",
          alignItems: "center",
          marginBottom: "20px",
          marginTop: "-250px",
          width: "100%",
        }}
      >
        <img
          src={require(`../../media/south_sudan_infographic.png`)}
          alt={"An infographic about the South Sudan refugee crisis"}
          className="center-block"
          style={{ height: "90%", width: "90%", marginLeft: "5%" }}
        />
      </div>
      <div>
        <Footer />
      </div>
    </div>
  );
}

export default HomePage;
