import React, { useState, useEffect } from "react";
import axios from "axios";
import { useLocation } from "react-router-dom";

import CountryModelCard from "../Countries/CountryModelCard";
import CharityModelCard from "../Orgs/CharityModelCard";
import NewsModelCard from "../News/NewsModelCard";

import "../Instance_Generics/InstancePage.css";

import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";

export interface CountryInstance {
  name: string;
  code: string;
  flag_url: string;
  chart: { [key: number]: any };
  refugees: number;
  asylum_seekers: number;
  population: number;
  distance: number;
  news_titles: [number];
  news_id: [number];
  org_names: [string];
  org_short_names: [string];
}

export interface CharitiesInstance {
  name: string;
  image: string;
  date_created: string;
  country: [string];
  type: string;
  description: string;
  homepage: string;
  status: string;
  news_id: [number];
  news: [string];
}

export interface NewsInstance {
  id: number;
  Title: string;
  Date: string;
  Sources: [string];
  Sources_Short: [string];
  Image: string;
  Countries: [string];
  Type: string;
  Text: string;
  Link: string;
  Map: { [key: number]: any };
}

const SearchPage = () => {
  const location = useLocation();
  const [searchQuery, setSearchQuery] = useState(location.state ? location.state.data.search : "")
  const [payload, fillPayload] = useState(location.state ? `&search=${encodeURIComponent(searchQuery)}` : "")
  const [totalItems, setTotalItems] = useState<number>(0);

  const numCardsPerPage = 8;
  const [currentPage, setCurrentPage] = useState(1);
  const [loaded, setLoaded] = useState(false);
  const [countryInstances, setCountryInstances] = useState<CountryInstance[]>([]);
  const [orgInstances, setOrgInstances] = useState<CharitiesInstance[]>([]);
  const [newsInstances, setNewsInstances] = useState<NewsInstance[]>([]);
  const [totalPages, setTotalPages] = useState(0);
  const [modelChosen, setModelChosen] = useState("countries")
  

  const handleSearch = async (e: React.FormEvent) => {
    e.preventDefault();

    fillPayload(`&search=${encodeURIComponent(searchQuery)}`)
    setCurrentPage(1);
  };

  const change_model = (model: string) => {
    setModelChosen(model)
    setCurrentPage(1)
  }
  const nextPage = () => {
    setCurrentPage((prevPage) => Math.min(prevPage + 1, Math.ceil(totalItems / numCardsPerPage)));
  };

  const prevPage = () => {
    setCurrentPage((prevPage) => Math.max(prevPage - 1, 1));
  };

  const goToPage = (pageNumber: number) => {
    setCurrentPage(pageNumber);
  };

  useEffect(() => {
    axios 
      .get(
        `https://api.supportsouthsudan.me/${modelChosen}?page=${currentPage}&limit=${numCardsPerPage}${payload}`
      )

      .then((response) => {
        if (modelChosen === "orgs"){
          setOrgInstances(response.data.orgs);
        } else if (modelChosen === "countries"){
          setCountryInstances(response.data.countries);
        } else {
          setNewsInstances(response.data.news);
        }
        setTotalItems(response.data._metadata.total)
        setTotalPages(Math.ceil(totalItems / numCardsPerPage))
        setLoaded(true);
      })
      .catch((error) => {
        console.log("Error", error);
        setLoaded(true);
      });
    setLoaded(true);
  }, [currentPage, modelChosen, payload, totalItems]);

  if (!loaded) {
    return (
      <h1 style={{ textAlign: "center", color: "#e8ddd8" }}>Page Loading...</h1>
    );
  }

  return (
    <div className="container mt-4">
      <form
        onSubmit={handleSearch}
        className="search-form"
        style={{ margin: "20px auto", maxWidth: "1100px" }}
      >
        <div className="input-group mb-3">
          <input
            type="text"
            className="form-control"
            placeholder="Search..."
            value={searchQuery}
            onChange={(e) => setSearchQuery(e.target.value)}
            style={{ flex: "4" }}
          />
          <button
            type="submit"
            className="btn btn-secondary"
            style={{
              backgroundColor: "#60755a",
              borderColor: "#60755a",
              color: "white",
            }}
          >
            Search
          </button>
        </div>
      </form>
      <div style={{ textAlign: "center", marginBottom: "20px" }}>
        <Button
          variant="dark"
          onClick={() => change_model("countries")}
          style={{ marginRight: "10px" }}
        >
          Countries
        </Button>
        <Button
          variant="dark"
          onClick={() => change_model("orgs")}
          style={{ marginLeft: "10px" }}
        >
          Organizations
        </Button>
        <Button
          variant="dark"
          onClick={() => setModelChosen("news")}
          style={{ marginLeft: "10px" }}
        >
          News
        </Button>
      </div>
      <h1 style={{ textAlign: "center" }}>Search Results:</h1>
      <p style={{ textAlign: "center" }}>Total Instances: {totalItems}</p>
      <p style={{ textAlign: "center" }}>Total Pages: {totalPages}</p> <br />
      {/* Display total items */}
      <Row className="justify-content-center">
          {modelChosen === "countries" && countryInstances.map((instance) => {
              return (
                <Col className="mb-3" key={instance.code}>
                  <CountryModelCard data={instance} searchQuery={searchQuery}/>
                </Col>
              );
            })}
          {modelChosen === "orgs" && orgInstances.map((instance) => {
              return (
                <Col className="mb-3" key={instance.name}>
                  <CharityModelCard data={instance} searchQuery={searchQuery}/>
                </Col>
              );
            })}
          {modelChosen === "news" && newsInstances.map((instance) => {
              return (
                <Col className="mb-3" key={instance.id}>
                  <NewsModelCard data={instance} searchQuery={searchQuery}/>
                </Col>
              );
            })}
        </Row>
      <div style={{ textAlign: "center", marginBottom: "20px" }}>
        <Button
          variant="dark"
          onClick={prevPage}
          disabled={currentPage === 1}
          style={{ marginRight: "10px" }}
        >
          Prev
        </Button>
        {Array.from({ length: Math.ceil(totalItems / numCardsPerPage) }, (_, i) => i + 1).map(
          (pageNumber) => (
            <Button
              key={pageNumber}
              variant={currentPage === pageNumber ? "light" : "light"}
              style={
                currentPage === pageNumber
                  ? { backgroundColor: "lightgray", marginRight: "10px" }
                  : { backgroundColor: "white", marginRight: "10px" }
              }
              onClick={() => goToPage(pageNumber)}
            >
              {pageNumber}
            </Button>
          )
        )}
        <Button
          variant="dark"
          onClick={nextPage}
          disabled={currentPage === Math.ceil(totalItems / numCardsPerPage)}
          style={{ marginLeft: "10px" }}
        >
          Next
        </Button>
      </div>
    </div>
  );
};

export default SearchPage;
