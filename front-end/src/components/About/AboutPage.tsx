import React, { useState, useEffect } from "react";
import AboutCard from "./AboutCard";
import ToolCard from "./ToolCard";
import DataSourceCard from "./DataSourceCard";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Footer from "../Header_and_Footer/Footer";

import {
  Member,
  members,
  updateAllCommits,
  updateAllIssues,
} from "../../gitlab_api";

const toolsList = [
  {
    name: "React",
    image: "react_logo.png",
    desc: "React is a free, popular JavaScript library for building interactive and reusable UI components for web and mobile apps.",
    link: "https://react.dev/",
  },
  {
    name: "Postman",
    image: "postman_logo.png",
    desc: "Postman empowers API development with design, testing, collaboration, and governance tools, all in one platform.",
    link: "https://www.postman.com/",
  },
  {
    name: "Gitlab",
    image: "gitlab_logo.png",
    desc: "GitLab offers a unified platform for DevOps teams, combining version control, issue tracking, CI/CD, and security features for streamlined software development and delivery.",
    link: "https://about.gitlab.com/",
  },
  {
    name: "AWS Amplify",
    image: "awsamplify_logo.png",
    desc: "AWS Amplify simplifies full-stack app development by providing tools and services to connect your frontend to cloud features like authentication, storage, APIs, and deployment.",
    link: "https://aws.amazon.com/amplify/",
  },
  {
    name: "NameCheap",
    image: "namecheap_logo.png",
    desc: "Namecheap is a one-stop shop for bringing your online ideas to life, offering domain registration, web hosting, email, security, and website building tools at affordable prices. ",
    link: "https://www.namecheap.com/",
  },
  {
    name: "Bootstrap",
    image: "bootstrap_logo.png",
    desc: "Bootstrap is a free and open-source CSS framework for faster and easier web development, featuring pre-built components like buttons, menus, and forms, for creating responsive and mobile-friendly websites.",
    link: "https://getbootstrap.com/",
  },
];

function AboutPage() {
  const [membersData, setMembersData] = useState(Array<Member>);
  const [totalCommits, setTotalCommits] = useState(0);
  const [totalIssues, setTotalIssues] = useState(0);

  async function updateCommitInfo() {
    setTotalCommits(0);
    let total = await updateAllCommits();
    setTotalCommits(total);
    setMembersData(members);
  }

  async function updateIssuesInfo() {
    setTotalIssues(0);
    let total = await updateAllIssues();
    setTotalIssues(total);
    setMembersData(members);
  }

  useEffect(() => {
    updateCommitInfo();
    updateIssuesInfo();
  }, []);

  const sources = [
    {
      name: "UNHCR ODP",
      image: "UNHCR_ODP.png",
      desc: "",
      link: "https://data.unhcr.org/en/situations/southsudan",
    },
    {
      name: "ReliefWeb",
      image: "relief_web_logo.png",
      desc: "",
      link: "https://reliefweb.int/updates?view=headlines&search=south+sudan+refugee",
    },
    {
      name: "UNHCR Data Finder",
      image: "UNHCR_logo.png",
      desc: "",
      link: "https://www.unhcr.org/refugee-statistics/download/?url=jwA88D",
    },
    {
      name: "UNHCR ODP API",
      image: "UNHCR_ODP.png",
      desc: "",
      link: "https://data.unhcr.org/api/doc#/",
    },
    {
      name: "REST Countries",
      image: "REST_countries_logo.png",
      desc: "",
      link: "https://restcountries.com/",
    },
    {
      name: "News API",
      image: "news_api_logo.png",
      desc: "",
      link: "https://newsapi.org/",
    },
    {
      name: "Youtube API",
      image: "youtube_logo.png",
      desc: "",
      link: "https://developers.google.com/youtube/v3/",
    },
    {
      name: "Bing API",
      image: "bing_logo.png",
      desc: "",
      link: "https://www.microsoft.com/en-us/bing/apis/bing-web-search-api",
    },
    {
      name: "Google Maps API",
      image: "google_maps_logo.png",
      desc: "",
      link: "https://developers.google.com/maps",
    },
  ];

  return (
    <div style={{ color: "#FF4928" }}>
      <Container className="title text-center mt-5 mb-4">
        <h1>About SupportSouthSudan</h1>
      </Container>
      <Container
        className="container text-start mt-5 mb-4"
        style={{ color: "#d6c7bf" }}
      >
        <p>
          SupportSouthSudan is a comprehensive online platform dedicated to
          fostering understanding and awareness of the South Sudanese refugee
          crisis. The site serves as a hub for information regarding South
          Sudanese refugees, including details about the countries that have
          accepted them, associated charities and organizations involved in
          their support, and relevant news articles covering the situation.
        </p>
        <p>
          The primary purpose of SupportSouthSudan is to increase awareness
          about the experiences of South Sudanese refugees, shedding light on
          the challenges they face and the resilience they demonstrate in the
          face of adversity. By highlighting the crucial role played by
          charities and organizations in assisting this community, the platform
          aims to inspire support and action from individuals, governments, and
          international entities to help this group of people.
        </p>
        <p>
          Intended users of SupportSouthSudan include the general public:
          individuals interested in learning more about the South Sudanese
          refugee crisis; activists and advocates: those passionate about
          refugee rights and humanitarian causes; and donors and
          philanthropists: individuals and organizations looking to contribute
          financial or material resources to support South Sudanese refugees and
          the charities/organizations assisting them.
        </p>
        <p>
          Integrating disparate data from various sources related to South
          Sudanese refugees has the potential to yield several interesting
          results. Users can gain a comprehensive understanding of the crisis by
          exploring different aspects, including its magnitude, root causes, and
          ongoing relief efforts. Access to diverse data sources enables
          insights into refugee experiences, ranging from demographic data to
          personal stories and living conditions in refugee camps, fostering
          empathy and understanding. Analyzing these data sets can reveal trends
          and patterns related to displacement, aid allocation, and resettlement
          outcomes, informing decision-making processes.
        </p>
      </Container>
      <Container className="container text-center">
        <br />
        <button
          onClick={() =>
            window.open(
              "https://youtu.be/7R-PdeqDFVQ",
              "_blank"
            )
          }
          className="btn btn-dark"
          style={{
            width: "500px",
            backgroundColor: "#60755a",
            borderColor: "#60755a",
            color: "white",
          }}
        >
          Pecha Kucha Presentation
        </button>
      </Container>
      <Container className="title text-center mt-5 mb-4">
        <h1>Meet the Team</h1>
      </Container>
      <Container fluid>
        <Row className="d-flex justify-content-center align-items-center mt-5 mb-4">
          {membersData.map((card, index) => (
            <Col key={index} xs={12} sm={6} md={4} lg={2} className="col">
              <AboutCard AboutCardData={card} />
            </Col>
          ))}
        </Row>
      </Container>
      <Container
        className="container text-center mt-5 mb-4"
        style={{ color: "#d6c7bf" }}
      >
        <h2 className="title" style={{ color: "#FF4928" }}>GitLab Statistics</h2>
        <br />
        <h3>
          <b>Total Commits Created:</b> {Math.floor(totalCommits / 2)}
        </h3>
        <h3>
          <b>Total Issues Closed:</b> {totalIssues }
        </h3>
        <h3>
          <b>Total Tests Written:</b> 19
        </h3>
      </Container>
      <Container className="container text-center">
        <br />
        <button
          onClick={() =>
            window.open(
              "https://winter-resonance-655151.postman.co/workspace/My-Workspace~710eae5c-8658-47b0-80e7-a45423968684/collection/32889511-60435d0a-6c33-45fc-b72e-979f0b2fe29e?action=share&creator=32897182",
              "_blank"
            )
          }
          className="btn btn-dark"
          style={{
            width: "500px",
            backgroundColor: "#60755a",
            borderColor: "#60755a",
            color: "white",
          }}
        >
          API Documentation
        </button>
      </Container>
      <Container className="title text-center mt-5 mb-4">
        <h2>Tools We Used</h2>
      </Container>
      <Container fluid>
        <Row className="d-flex justify-content-center align-items-center mt-5 mb-4">
          {toolsList.map((card, index) => (
            <Col
              key={index}
              xs={12}
              sm={6}
              md={4}
              lg={2}
              xl={2}
              className="col"
            >
              <ToolCard ToolCardData={card} />
            </Col>
          ))}
        </Row>
      </Container>
      <Container className="container mt-6 mb-4">
        <h2 className="title text-center mt-5 mb-4">Data Sources</h2>
        <Row className="d-flex justify-content-center mt-5 mb-4">
          {sources.map((source, index) => (
            <Col
              key={index}
              xs={12}
              sm={6}
              md={4}
              lg={3}
              xl={2}
              className="col"
            >
              <DataSourceCard ToolCardData={source} />
            </Col>
          ))}
        </Row>
      </Container>
      <div>
        <Footer />
      </div>
    </div>
  );
}

export default AboutPage;
