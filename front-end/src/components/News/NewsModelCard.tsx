import React from "react";
import { useNavigate } from "react-router-dom";

interface CardProps {
  id: number;
  Title: string;
  Date: string;
  Sources: [string];
  Sources_Short: [string];
  Image: string;
  Countries: [string];
  Type: string;
  Text: string;
  Link: string;
  Map: { [key: number]: any };
}

interface CardComponentProps {
  data: CardProps;
  searchQuery: string;
}

const NewsModelCard: React.FC<CardComponentProps> = ({ data, searchQuery }) => {
  const navigate = useNavigate();

  const highlightSearchQuery = (text: string, query: string) => {
    if (!text || !query.trim()) return text; // Add null check here
    const regex = new RegExp(`(${query})`, 'gi');
    return text.replace(regex, '<mark>$1</mark>');
  };
  
  return (
    <div className="col-md-4 mb-4" data-testid="news-model-card">
      <div
        className="card p-3 border rounded-3 shadow"
        onClick={() =>
          navigate(`/news/${data.Title.replaceAll('/', '')}`, { state: { data: data } })
        }
        style={{ width: "300px", height: "500px", backgroundColor: "#e8ddd8" }}
      >
        <img
          src={data.Image}
          className="card-img-top"
          style={{ height: "200px", objectFit: "cover" }}
          alt={data.Image}
        />
        <div
          className="card-body"
          style={{ maxHeight: "600px", overflowY: "auto" }}
        >
          <p className="card-title">{data.Title}</p>
          <br />
          <h5 className="card-text">Published: <span dangerouslySetInnerHTML={{ __html: highlightSearchQuery(data.Date, searchQuery) }} /></h5>
          <h5 className="card-text">Sources: <span dangerouslySetInnerHTML={{ __html: highlightSearchQuery(data.Sources_Short[0], searchQuery) }} /></h5>
          <h5 className="card-text">Type of news: <span dangerouslySetInnerHTML={{ __html: highlightSearchQuery(data.Type, searchQuery) }} /></h5>
          <h5 className="card-text">
            Countries:
            {data.Countries.map((country, index) => (
              <span key={index}>
                {highlightSearchQuery(country.trim().replace("*", ""), searchQuery)}
                {index !== data.Countries.length - 1 ? ", " : ""}
              </span>
            ))}
          </h5>
        </div>
      </div>
    </div>
  );
};

export default NewsModelCard;
