
const news_types = ["Situation Report", "News and Press Release", "Analysis", "Map", "Assessment", "Infographic", "Other", "Appeal", "Interactive", "Evaluation and Lessons Learned", "Manual and Guideline", "UN Document"]

const news_country = [ "Haiti", "United Kingdom of Great Britain and Northern Ireland", "Uruguay", "Costa Rica", "Serbia", "Israel",
        "Dominica", "Turkmenistan", "Bolivia (Plurinational State of)", "Nicaragua", "Algeria", "Liberia", "Timor-Leste", "Gabon", "Morocco", "Germany", "India", "Qatar", "Gambia", "Cambodia", "Paraguay", "Nigeria", "Malawi", "Djibouti", "Saint Vincent and the Grenadines",
        "Georgia", "Western Sahara", "Trinidad and Tobago", "Libya", "Mauritania", "Jordan", "Peru", "Belize", "Myanmar", "Benin", "Burundi", "Egypt", "United States of America", "Ghana", "Angola", "Uzbekistan", "Ivory Coast", "Palestine, State of", "Lebanon", "Swaziland", "Somalia",
        "Sri Lanka", "Brazil", "Guatemala", "Ethiopia", "Sudan", "Congo (Democratic Republic of the)", "Lesotho", "Azerbaijan", "Armenia", "Maldives", "Colombia", "Cabo Verde", "Equatorial Guinea", "Vietnam", "Burkina Faso", "South Africa", "Argentina", "Barbados", "Zimbabwe", "Mauritius", "Belarus",
        "Korea (Democratic People's Republic of)", "Panama", "Sierra Leone", "Brunei Darussalam", "Indonesia", "Pakistan", "Papua New Guinea", "Ukraine", "Turkey", "Tanzania, United Republic of", "China", "Thailand", "Niger", "Venezuela (Bolivarian Republic of)", "Saudi Arabia", "Greece",
        "Japan", "Iceland", "Madagascar", "Togo", "Russian Federation", "Korea (Republic of)", "Honduras", "Ireland", "Botswana", "Guinea-Bissau", "Cuba", "Bangladesh", "Malaysia", "Fiji", "Saint Kitts and Nevis", "Kazakhstan", "Grenada", "Cameroon", "Senegal", "Dominican Republic", "Romania",
        "Lao People's Democratic Republic", "Mali", "Seychelles", "Italy", "Bahrain", "Philippines", "Kenya", "Kyrgyzstan", "Eritrea", "Zambia", "Rwanda", "Oman", "Namibia", "Mexico", "Sao Tome and Principe", "Malta", "Tunisia", "Guinea", "Uganda", "Iraq", "Comoros", "Nepal",
        "Bhutan", "United Arab Emirates", "El Salvador", "Central African Republic", "Chad", "Tajikistan", "Congo", "Canada", "Iran (Islamic Republic of)", "Moldova (Republic of)", "Yemen", "Afghanistan", "Spain", "South Sudan", "Mozambique", "Ecuador", "Kuwait", "Syrian Arab Republic"]

const news_source = ["CCCM Cluster", "HRW", "NRC", "OCHA", "Internews", "GPEI", "UNMISS",
        "Logistics Cluster", "UN DPO", "Health Cluster", "Govt. Uganda", "GAVI", "IRC", "Protection Cluster", "World Bank", "UN HRC", "IFRC",
        "Govt. UK", "NGOWG", "UNHCR", "RJMEC", "UNISFA", "AVSI", "FAO", "AGRA", "MAG", "Govt. Netherlands", "Watchlist",
        "REDRESS", "Malteser", "UN SC", "Insecurity Insight", "REACH", "WMO", "Radio Dabanga", "Forum-Asia", "GCR2P", "FSNWG", "PAX",
        "FEWS NET", "WFP", "USAID", "Govt. South Sudan", "RI", "WHO", "IOM"]

const orgs_type = ["Red Cross/Red Crescent Movement", "Academic and Research Institution", "Other","Government", "Media", "Non-governmental Organization", "International Organization"]

const master_list = [news_types, news_country, news_source, orgs_type]

export default master_list