import { useState, useEffect } from "react";
import NewsModelCard from "./NewsModelCard";
import Footer from "../Header_and_Footer/Footer";

import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";

import axios from "axios";
import master_list from "./FilterRef";

interface NewsInstance {
  id: number;
  Title: string;
  Date: string;
  Sources: [string];
  Sources_Short: [string];
  Image: string;
  Countries: [string];
  Type: string;
  Text: string;
  Link: string;
  Map: { [key: number]: any };
}

function NewsPage() {
  const numCardsPerPage = 8;
  const [totalInstances, setTotalInstances] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [loaded, setLoaded] = useState(false);
  const [totalPages, setTotalPages] = useState(0);
  const [newsInstances, setNewsInstances] = useState<NewsInstance[]>([]);
  const [payload, fillPayload] = useState("")

  const nextPage = () => {
    setCurrentPage((prevPage) => Math.min(prevPage + 1, totalPages));
  };

  const prevPage = () => {
    setCurrentPage((prevPage) => Math.max(prevPage - 1, 1));
  };

  const goToPage = (pageNumber: number) => {
    setCurrentPage(pageNumber);
  };

  // const navigate = useNavigate();
  const [searchQuery, setSearchQuery] = useState("");
  const [sortBy, setSortBy] = useState("");
  const [newsType, setNewsType] = useState("");
  const [newsSource, setNewsSource] = useState("");
  const [newsCountry, setNewsCountry] = useState("");

  const handleSearch = async (event: React.FormEvent) => {
    event.preventDefault();

    fillPayload(`&search=${encodeURIComponent(searchQuery)}&sort=${sortBy}&type=${newsType}&source=${newsSource}&country_name=${newsCountry}`)
    setCurrentPage(1);
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(
          `https://api.supportsouthsudan.me/news?page=${currentPage}&limit=${numCardsPerPage}${payload}`
        );
        setNewsInstances(response.data.news);
        setTotalInstances(response.data._metadata.total)
        setTotalPages(Math.ceil(totalInstances/numCardsPerPage))
        setLoaded(true);
      } catch (error) {
        console.error("Error", error);
        setLoaded(true);
      }
    };

    fetchData();
  }, [currentPage, payload, totalInstances]);

  if (!loaded) {
    return (
      <h1 style={{ textAlign: "center", color: "#e8ddd8" }}>Page Loading...</h1>
    );
  }
  //console.log(newsInstances);
  return (
    <div>
      <div className="container mt-5" style={{ color: "#e8ddd8" }}>
        <h1 className="title text-center mb-4" style={{ color: "#FF4928" }}>
          News
        </h1>
        <p>
          An aggregated and organized collection of relevant news articles and
          updates related to South Sudanese refugees. It encompasses a variety
          of sources, including mainstream media, humanitarian organizations,
          governmental agencies, and independent journalism, to offer a
          comprehensive overview of current events and developments affecting
          the refugee community. This model aims to keep stakeholders informed
          about the latest developments, challenges, and progress in addressing
          the refugee crisis, fostering empathy, and promoting advocacy efforts.
        </p>
        <form
          onSubmit={handleSearch}
          className="search-form"
          style={{ margin: "20px auto", maxWidth: "1100px" }}
        >
          <div className="input-group mb-3">
            <input
              type="text"
              className="form-control"
              placeholder="Search..."
              value={searchQuery}
              onChange={(e) => setSearchQuery(e.target.value)}
              style={{ flex: "4" }}
            />
            <select
              className="form-select"
              value={newsType}
              onChange={(e) => setNewsType(e.target.value)}
              style={{ flex: "1" }}
            >
              <option value="">News Type</option>
              {master_list[0].map((type) => (
                <option key={type} value={`${type}`}>{type}</option>
              ))}
            </select>
            <select
              className="form-select"
              value={newsSource}
              onChange={(e) => setNewsSource(e.target.value)}
              style={{ flex: "1" }}
            >
              <option value="">Source</option>
              {master_list[2].map((type) => (
                <option key={type} value={`${type}`}>{type}</option>
              ))}
            </select>
            <select
              className="form-select"
              value={newsCountry}
              onChange={(e) => setNewsCountry(e.target.value)}
              style={{ flex: "1" }}
            >
              <option value="">Country</option>
              {master_list[1].map((type) => (
                <option key={type} value={`${type}`}>{type}</option>
              ))}
            </select>
            <select
              className="form-select"
              value={sortBy}
              onChange={(e) => setSortBy(e.target.value)}
              style={{ flex: "1" }}
            >
              <option value="">Sort by</option>
              <option value="title">Title</option>
              <option value="date">Date</option>
            </select>
            <button
              type="submit"
              className="btn btn-secondary"
              style={{
                backgroundColor: "#60755a",
                borderColor: "#60755a",
                color: "white",
              }}
            >
              Search
            </button>
          </div>
        </form>
        <p>- Instances in this model: {totalInstances}</p>
        <p>- Number of pages {totalPages}</p>
        <p>- Current page: {currentPage}</p>
        <br />
        <Row
          className="d-flex justify-content-center"
          xs={1}
          md={3}
          lg={3}
          xlg={4}
        >
          {newsInstances &&
            newsInstances.map((instance) => {
              return (
                <Col className="mb-3" key={instance.id}>
                  <NewsModelCard data={instance} searchQuery={searchQuery}/>
                </Col>
              );
            })}
        </Row>
        <div style={{ textAlign: "center", marginBottom: "20px" }}>
          <Button
            variant="dark"
            onClick={prevPage}
            disabled={currentPage === 1}
            style={{ marginRight: "10px" }}
          >
            Prev
          </Button>
          {Array.from({ length: totalPages }, (_, i) => i + 1).map(
            (pageNumber) => (
              <Button
                key={pageNumber}
                variant={currentPage === pageNumber ? "light" : "light"}
                style={
                  currentPage === pageNumber
                    ? { backgroundColor: "lightgray", marginRight: "10px" }
                    : { backgroundColor: "white", marginRight: "10px" }
                }
                onClick={() => goToPage(pageNumber)}
              >
                {pageNumber}
              </Button>
            )
          )}
          <Button
            variant="dark"
            onClick={nextPage}
            disabled={currentPage === totalPages}
            style={{ marginLeft: "10px" }}
          >
            Next
          </Button>
        </div>
      </div>
      <div>
        <Footer />
      </div>
    </div>
  );
}

export default NewsPage;
