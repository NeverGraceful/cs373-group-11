import React from "react";
import { GoogleMap, MarkerF } from "@react-google-maps/api";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import "../Instance_Generics/InstancePage.css";
import InstanceCard from "../Instance_Generics/InstanceCard";
import { Link, useLocation } from "react-router-dom";
import { Button } from "react-bootstrap";

export interface NewsInstanceData {
  id: number;
  Title: string;
  Date: string;
  Sources: [string];
  Sources_Short: [string];
  Image: string;
  Countries: string[];
  Type: string;
  Text: string;
  Link: string;
  Map: { [key: number]: any };
}

const NewsInstance = () => {
  const location = useLocation();
  const center = {
    lat: location.state.data.Map.lat,
    lng: location.state.data.Map.long,
  };
  return (
    <div className="container mt-4">
      <h1 className="title text-center mb-4">{location.state.data.Title}</h1>
      <div className="row" style={{ marginTop: "50px" }}>
        <div className="col-md-6">
          <img
            src={location.state.data.Image}
            alt="News"
            className="img-fluid"
          />
          <div className="description-box mb-4">
            <div className="description-content" style={{ marginTop: "30px" }}>
              {location.state.data.Text}
            </div>
          </div>
        </div>
        <div className="col-md-6">
          <div className="data-box">
            <div className="data-row">
              <span className="data-label">Published: </span>
              <span className="data-value">{location.state.data.Date}</span>
            </div>
            <br />
            <div className="data-row">
              <span className="data-label">Type of article: </span>
              <span className="data-value">{location.state.data.Type}</span>
            </div>
            <br />
            <br />
            <div
              className="data-row d-flex justify-content-center"
              style={{ width: "350px", height: "250px" }}
            >
              <GoogleMap
                mapContainerStyle={{
                  width: "100%",
                  height: "100%",
                }}
                zoom={10}
                center={{ lat: center.lat, lng: center.lng }}
              >
                <MarkerF position={{ lat: center.lat, lng: center.lng }} />
              </GoogleMap>
            </div>
          </div>
        </div>
      </div>
      <Button variant="outline-success" style={{backgroundColor: "#60755a", borderColor: "#60755a", color: "white"}}>
        <Link
          style={{ color: "black", textDecoration: "inherit" }}
          to={`${location.state.data.Link}`}
        >
          More Info
        </Link>
      </Button>
      <div className="row" style={{ marginTop: "150px" }}>
        <div className="row-md-6">
          <div className="data-row">
            <span className="data-label">Sources: </span>
            <Row>
              {location.state.data.Sources_Short &&
                location.state.data.Sources_Short.map(
                  (sourceWord: string, index: number) => {
                    if (sourceWord) {
                      const trimmed = sourceWord.trim();
                      if (trimmed) {
                        return (
                          <Col key={index} xs={6} md={4} lg={3}>
                            <InstanceCard
                              instanceCardData={{
                                name: trimmed,
                                link: `../../orgs/${trimmed}`,
                                inst_type: "orgs",
                                news_id: -1,
                              }}
                            />
                            {/* <InstanceCard instanceCardData={{ name:  trimmed}} /> */}
                          </Col>
                        );
                      }
                    } else {
                      return (
                        <Col>
                          <h1 style={{ textAlign: "center" }}>
                            Connection not found
                          </h1>
                          ;
                        </Col>
                      );
                    }
                    return null;
                  }
                )}
            </Row>
          </div>
        </div>
        <div className="row-md-6">
          <div className="data-row">
            <span className="data-label">Countries: </span>
            <Row>
              {location.state.data.Countries &&
                location.state.data.Countries.slice(0, 5).map(
                  (country: string, index: number) => {
                    const trimmed = country.trim();
                    if (trimmed) {
                      var new_trimmed = trimmed.replace("*", "");
                      return (
                        <Col key={index} xs={6} md={4} lg={3}>
                          <InstanceCard
                            instanceCardData={{
                              name: new_trimmed,
                              link: `../../countries/${new_trimmed}`,
                              inst_type: "countries",
                              news_id: -1,
                            }}
                          />
                        </Col>
                      );
                    }
                    return null;
                  }
                )}
              {location.state.data.Countries.length === 0 && (
                <Col>
                  <h1 style={{ textAlign: "center" }}>Connection not found</h1>;
                </Col>
              )}
            </Row>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NewsInstance;
