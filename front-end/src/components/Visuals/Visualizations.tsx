import React, { useState, useEffect } from 'react';
import OrgTypePieChart from './OrgTypePieChart';
import CountryPeopleLineGraph from './PeopleMovingLineGraph';
import NewsSourceBarChart from './NewsSourceBubble';
import "../Instance_Generics/InstancePage.css";
import Footer from '../Header_and_Footer/Footer';

function Visualizations() {
  const [pieChartLoaded, setPieChartLoaded] = useState(false);
  const [lineGraphLoaded, setLineGraphLoaded] = useState(false);
  const [barChartLoaded, setBarChartLoaded] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      try {
        // Simulated async operations
        await new Promise(resolve => setTimeout(resolve, 1000)); // Simulate data fetching
        setPieChartLoaded(true);

        await new Promise(resolve => setTimeout(resolve, 2000)); // Simulate data fetching
        setLineGraphLoaded(true);

        await new Promise(resolve => setTimeout(resolve, 3000)); // Simulate data fetching
        setBarChartLoaded(true);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, []);

  return (
    <div>
      <div className="container">
        {pieChartLoaded && <OrgTypePieChart />}
        {!pieChartLoaded && <p>Loading pie chart...</p>}
      </div>
      <br />
      <div className="container">
        {lineGraphLoaded && <CountryPeopleLineGraph />}
        {!lineGraphLoaded && <p>Loading line graph...</p>}
      </div>
      <br />
      <div className="container">
        {barChartLoaded && <NewsSourceBarChart />}
        {!barChartLoaded && <p>Loading bar chart...</p>}
      </div>
      <br></br>
      <br></br>
      <br></br>
      <div style={{marginLeft: 100, marginRight: 100}}>
        <h2 style={{color: "white"}}>Self-Critiques for SupportSouthSudan</h2>
        <h3 style={{color: "white"}}>What did we do well?</h3>
        <p style={{color: "white"}}>
          We like the way this website looks and works. The layout is clean and well-organized, making it 
          easy to find what you need. The visuals are nice and pair well with the informative content. The 
          models make a lot of sense in our context and their connections as well.  We could navigate smoothly, 
          test out various search queries, and get highly relevant results without any issues.You can both sort 
          and filter to do more complex searches if needed. Additionally, searching has results organized into 
          their respective models. Connections between items are displayed the same way you would find on the 
          model page. Overall, our group did a great job designing an intuitive, user-friendly website that is 
          highly informative.
        </p>
        <h3 style={{color: "white"}}>What did we learn?</h3>
        <p style={{color: "white"}}>
          Over the course of the semester, we have learned and practiced skills that allow us to better 
          collaborate with one another. We began to communicate more things, from blocks in development 
          to real-life obstacles that held up the delivery of features. We became more diligent in the 
          work we did, setting realistic goals and deadlines for ourselves. Time management and task 
          prioritization were key learnings that helped us work more efficiently as a team. We also grew 
          in our ability to give and receive constructive feedback, allowing us to iterate and improve upon 
          our work continuously.
        </p>
        <h3 style={{color: "white"}}>What did we teach each other?</h3>
        <p style={{color: "white"}}>
          As developers we learned how to give context to one another in order to receive help when 
          debugging. As there are areas of code that not everyone touches, in order to be able to help, 
          a good explanation is needed. In addition, we teach each other our needs during development. 
          For example, the front-end dictates what information is needed from the back-end. Then, the 
          back-end teaches the front-end how that data is structured in the JSON. 
        </p>
        <h3 style={{color: "white"}}>What can we do better?</h3>
        <p style={{color: "white"}}>
          From a user’s perspective, we could do better at the website’s ability to look nice while the 
          window size gets smaller. There are some instances of the models that aren’t as relevant to 
          South Sudan. For example, Albania doesn’t have any refugees or asylum seekers from South Sudan, 
          and no news or org connections.
        </p>
        <h3 style={{color: "white"}}>What effect did the peer reviews have?</h3>
        <p style={{color: "white"}}>
          The peer reviews gave us insight on areas where we can improve as teammates. They provided 
          constructive feedback on our collaboration skills, communication styles, and ability to meet 
          deadlines. The reviews highlighted our strengths as a team, such as our dedication to the project 
          and our willingness to help each other out. This feedback was invaluable as it allowed us to reflect 
          on our teamwork and identify specific areas for improvement.
        </p>
        <h3 style={{color: "white"}}>What puzzles us?</h3>
        <p style={{color: "white"}}>
          At the end of this project, most of what puzzled us has been solved and implemented by now. 
          One thing that was recently puzzling was dealing with YouTube API keys running out of quota 
          for each of our organizations. However, a solution to this would be to store the video url 
          while scripting so that there aren’t recurring API requests. Another thing would be how we 
          could scale to include more instances of news and organizations and what problems could occur 
          from that
        </p>
      </div>
      <div>
        <Footer />
      </div>
    </div>
  );
}


export default Visualizations;