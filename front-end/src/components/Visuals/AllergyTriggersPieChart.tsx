import React, { useEffect, useState, useRef } from 'react';
import axios from 'axios';
import * as d3 from 'd3';

interface ApiResponse {
    allergyInfo: {
        triggers: string;
    }[];
}

export async function fetchAllergyTriggers(): Promise<string[]> {
    const url = "https://platform.safeats4you.lol//allergyInfo?page=1&per_page=200";
    try {
        const response = await axios.get<ApiResponse>(url);
        if (response.data.allergyInfo.length > 0) {
            let results = [];
            for (let i = 0; i < response.data.allergyInfo.length; i++) {
                if ( Object.keys(response.data.allergyInfo[i].triggers).length){
                    results.push(response.data.allergyInfo[i].triggers)
                }
            }
            console.log(results)
            return results;
        } else {
            console.error("No allergy items found in the response.");
            return [];
        }
    } catch (error) {
        console.error("Error fetching allergy triggers:", error);
        return [];
    }
}


export function countTypes(types: string[]): Map<string, number> {
    const sourceMap = new Map<string, number>();
    types.forEach(type => {
        const count = sourceMap.get(type) || 0;
        sourceMap.set(type, count + 1);
    });
    return sourceMap;
}

interface PieChartData {
    type: string;
    count: number;
}

export async function createPieChart(data: PieChartData[]): Promise<SVGSVGElement> {
    const width = 700;
    const height = 700;

    const color = d3.scaleOrdinal<string>(d3.schemeCategory10);

    const pie = d3.pie<PieChartData>()
        .sort(null)
        .value(d => d.count);

    const arc = d3.arc<any>()
        .innerRadius(0)
        .outerRadius(Math.min(width, height) / 2 - 1);

    const arcs = pie(data);

    const svg = d3.create("svg")
        .attr("width", width)
        .attr("height", height)
        .attr("viewBox", [-width / 2, -height / 2, width, height])
        .attr("style", "max-width: 100%; height: auto; font: 10px sans-serif;");

    svg.append("g")
        .attr("stroke", "white")
        .selectAll("path")
        .data(arcs)
        .join("path")
        .attr("fill", (_, i) => color(i.toString())) // Assign colors based on index
        .attr("d", arc)
        .append("title")
        .text((d, i) => `${data[i].type}: ${data[i].count.toLocaleString("en-US")}`);

    svg.append("g")
        .attr("text-anchor", "middle")
        .selectAll("text")
        .data(arcs)
        .join("text")
        .attr("transform", d => `translate(${arc.centroid(d)})`)
        .call(text => text.append("tspan")
            .attr("y", "-0.4em")
            .attr("font-weight", "bold")
            .text((d, i) => data[i].type))
        .call(text => text.filter(d => (d.endAngle - d.startAngle) > 0.25).append("tspan")
            .attr("x", 0)
            .attr("y", "0.7em")
            .attr("fill-opacity", 0.7)
            .text((d, i) => data[i].count.toLocaleString("en-US")));

    return svg.node() as SVGSVGElement;
}


const AllergyTriggersPieChart: React.FC = () => {
    const [chartLoaded, setChartLoaded] = useState(false);
    const chartNodeRef = useRef<SVGSVGElement | null>(null);

    useEffect(() => {
        const fetchAndRenderChart = async () => {
            try {
                const allergies_triggers = await fetchAllergyTriggers();
                if (!allergies_triggers) {
                    console.error("No allergies found.");
                    return;
                }
                const typeMap = countTypes(allergies_triggers);
                const data = Array.from(typeMap.entries()).map(([type, count]) => ({ type, count }));
                const chartNode = await createPieChart(data);
                setChartLoaded(true);

                chartNodeRef.current = chartNode;

                const container = document.getElementById("pie-chart-container");
                if (container) {
                    container.appendChild(chartNode);
                } else {
                    console.error("Container not found.");
                }
            } catch (error) {
                console.error("An error occurred:", error);
            }
        };

        fetchAndRenderChart();

        return () => {
            const chartNode = chartNodeRef.current;
            if (chartNode) {
                chartNode.parentNode?.removeChild(chartNode);
            }
        };
    }, []);

    return (
        <div id="pie-chart-container">
            <h1>Allergy Triggers Pie Chart</h1>
            {!chartLoaded && <p>Loading chart...</p>}
        </div>
    );
};

export default AllergyTriggersPieChart;