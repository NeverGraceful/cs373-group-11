import React, { useEffect, useState, useRef } from 'react';
import axios from 'axios';
import * as d3 from 'd3';

interface ApiResponse {
    allergyInfo: {
        commonAgeGroup: string;
    }[];
}

export async function fetchAllergies(): Promise<string[]> {
    const url = "https://platform.safeats4you.lol//allergyInfo?page=1&per_page=200";
    try {
        const response = await axios.get<ApiResponse>(url);
        if (response.data.allergyInfo.length > 0) {
            let results = [];
            for (let i = 0; i < response.data.allergyInfo.length; i++) {
                if ( Object.keys(response.data.allergyInfo[i].commonAgeGroup).length){
                    results.push(response.data.allergyInfo[i].commonAgeGroup)
                }
            }
            console.log(results)
            return results;
        } else {
            console.error("No allergy items found in the response.");
            return [];
        }
    } catch (error) {
        console.error("Error fetching allergy sources:", error);
        return [];
    }
}

export function countAllergies(allergies: string[]): Map<string, number> {
    const allergyMap = new Map<string, number>();
    allergies.forEach(allergies => {
        const count = allergyMap.get(allergies) || 0;
        allergyMap.set(allergies, count + 1);
    });
    return allergyMap;
}

interface BarChartData {
    allergy: string;
    count: number;
}

export async function createBarChart(data: BarChartData[]): Promise<SVGSVGElement> {
    const margin = { top: 20, right: 0, bottom: 30, left: 40 };
    const width = 1700 - margin.left - margin.right;
    const height = 1000 - margin.top - margin.bottom;
    
    const svg = d3.create("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .attr("viewBox", [0, 0, width, height])
        .attr("style", "max-width: 100%; height: auto;");

    // Define scales
    const x = d3.scaleBand()
        .domain(data.map(d => d.allergy))
        .range([margin["left"], width - margin["right"]])
        .padding(0.1);

    const y = d3.scaleLinear()
        .domain([0, d3.max(data, d => d.count)!]) // Added '!' to ensure non-null
        .range([height - margin["bottom"], margin["top"]]);

    // Draw bars
    svg.append("g")
        .attr("fill", "steelblue")
        .selectAll()
        .data(data)
        .join("rect")
        .attr("x", d => x(d.allergy)!)
        .attr("y", d => y(d.count) - 30)
        .attr("height", d => (height - y(d.count)))
        .attr("width", x.bandwidth());

    // Draw x-axis
    svg.append("g")
        .attr("transform", `translate(0,${height - margin["bottom"]})`)
        .call(d3.axisBottom(x).tickSizeOuter(0));

    // Draw y-axis
    svg.append("g")
        .attr("transform", `translate(${margin["left"]},0)`)
        .call(d3.axisLeft(y))
        .call(g => g.select(".domain").remove())
        .call(g => g.append("text")
          .attr("x", -margin["left"])
          .attr("y", 10)
          .attr("fill", "currentColor")
          .attr("text-anchor", "start")
          .text("# of Reports"));

    return svg.node() as SVGSVGElement;
}

const AllergyByAgeBarChart: React.FC = () => {
    const [chartLoaded, setChartLoaded] = useState(false);
    const chartNodeRef = useRef<SVGSVGElement | null>(null);

    useEffect(() => {
        const fetchAndRenderChart = async () => {
            try {
                const allergies = await fetchAllergies();
                if (!allergies) {
                    console.error("No allergies found.");
                    return;
                }
                const allergyMap = countAllergies(allergies);
                const data = Array.from(allergyMap.entries()).map(([allergy, count]) => ({ allergy, count }));
                const chartNode = await createBarChart(data);
                setChartLoaded(true);

                chartNodeRef.current = chartNode;

                const container = document.getElementById("bar-chart-container"); // Changed to match the id in JSX
                if (container) {
                    container.appendChild(chartNode);
                } else {
                    console.error("Container not found.");
                }
            } catch (error) {
                console.error("An error occurred:", error);
            }
        };

        fetchAndRenderChart();

        // Cleanup function
        return () => {
            // Remove the chart when the component unmounts
            const chartNode = chartNodeRef.current;
            if (chartNode) {
                chartNode.parentNode?.removeChild(chartNode);
            }
        };
    }, []);

    return (
        <div id="bar-chart-container">
            <h1>Allergies by Age Bar Chart</h1>
            {!chartLoaded && <p>Loading chart...</p>}
        </div>
    );
}

export default AllergyByAgeBarChart;