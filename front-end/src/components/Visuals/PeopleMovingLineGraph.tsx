import React, { useEffect, useState, useRef } from 'react';
import axios from 'axios';
import * as d3 from 'd3';

interface ApiResponse {
    countries: {
        chart: Map<string, number[]>;
    }[];
}

export async function fetchPeopleMoved(): Promise<Map<string, number[]>[]> {
    const url = "https://api.supportsouthsudan.me/countries";
    try {
        const response = await axios.get<ApiResponse>(url);
        // Check if there are any items in the 'countries' array
        if (response.data.countries.length > 0) {
            // Return the chart data from the first item in the 'countries' array
            let results = [];
            for (let i = 0; i < response.data.countries.length; i++) {
                if ( Object.keys(response.data.countries[i].chart).length){
                    results.push(response.data.countries[i].chart)
                }
            }
            return results;
        } else {
            console.error("No news items found in the response.");
            return [];
        }
    } catch (error) {
        console.error("Error fetching news sources:", error);
        return [];
    }
}


export function grabData(data: Map<string, number[]>[]): [number, number][] {
    const dataMap = new Map<string, number>();
    data.forEach(country => {
        for (const [year, people] of Object.entries(country)){
            const currCount = dataMap.get(year) || 0;
            const addedArray = people || [0, 0];
            const addedCount = addedArray[0] + addedArray[1]
            dataMap.set(year, currCount + addedCount);
        }
    });
    console.log(dataMap)
    var coordArray: [number, number][] = []
    let keyList = dataMap.keys;
    console.log(keyList)
    dataMap.forEach((people, year) => {
        let newData: [number,number] = [0, 0];
        newData[0] = parseInt(year, 10)
        newData[1] = people / 100000
        coordArray.push(newData)
    })
    coordArray.sort()
    return coordArray;
}


export async function createGraph(data: [number, number][]): Promise<SVGSVGElement> {
    const width = 1500;
    const height = 800;
    const marginTop = 20;
    const marginRight = 30;
    const marginBottom = 30;
    const marginLeft = 40;

    // Declare the x (horizontal position) scale.
    const x = d3.scaleLinear()
        .domain([d3.min(data, d => d[0]) || 0, d3.max(data, d => d[0]) || 0])
        .range([marginLeft, width - marginRight]);

    // Declare the y (vertical position) scale.
    const y = d3.scaleLinear()
        .domain([0, d3.max(data, d => d[1]) || 0])
        .range([height - marginBottom, marginTop]);

    // Declare the line generator.
    const line = d3.line()
        .x(d => x(d[0]))
        .y(d => y(d[1]));

    // Create the SVG container.
    const svg = d3.create("svg")
        .attr("width", width)
        .attr("height", height)
        .attr("viewBox", [0, 0, width, height])
        .attr("style", "max-width: 100%; height: auto; height: intrinsic;");

    // Add the x-axis.
    svg.append("g")
        .attr("transform", `translate(0,${height - marginBottom})`)
        .call(d3.axisBottom(x).tickFormat(d3.format('d')).ticks(width / 80).tickSizeOuter(0));

    // Add the y-axis, remove the domain line, add grid lines and a label.
    svg.append("g")
        .attr("transform", `translate(${marginLeft},0)`)
        .call(d3.axisLeft(y).ticks(height / 40))
        .call(g => g.select(".domain").remove())
        .call(g => g.selectAll(".tick line").clone()
            .attr("x2", width - marginLeft - marginRight)
            .attr("stroke-opacity", 0.1))
        .call(g => g.append("text")
            .attr("x", -marginLeft)
            .attr("y", 10)
            .attr("fill", "currentColor")
            .attr("text-anchor", "start")
            .text("People displaced (In 100,000s)"));

    // Append a path for the line.
    svg.append("path")
        .attr("fill", "none")
        .attr("stroke", "steelblue")
        .attr("stroke-width", 1.5)
        .attr("d", line(data));

    return svg.node() as SVGSVGElement;
}


const CountryPeopleLineGraph: React.FC = () => {
    const [chartLoaded, setChartLoaded] = useState(false);
    const chartNodeRef = useRef<SVGSVGElement | null>(null);

    useEffect(() => {
        const fetchAndRenderChart = async () => {
            try {
                const moveData = await fetchPeopleMoved();
                if (!moveData) {
                    console.error("No sources found.");
                    return;
                }
                const data = grabData(moveData)
                const chartNode = await createGraph(data);
                setChartLoaded(true);

                chartNodeRef.current = chartNode;

                const container = document.getElementById("pie-chart-container");
                if (container) {
                    container.appendChild(chartNode);
                } else {
                    console.error("Container not found.");
                }
            } catch (error) {
                console.error("An error occurred:", error);
            }
        };

        fetchAndRenderChart();

        // Cleanup function
        return () => {
            // Remove the chart when the component unmounts
            const chartNode = chartNodeRef.current;
            if (chartNode) {
                chartNode.parentNode?.removeChild(chartNode);
            }
        };
    }, []);

    return (
        <div id="line-graph-container">
            <h1>Total People Displaced Over the Years</h1>
            {!chartLoaded && <p>Loading graph...</p>}
        </div>
    );
};

export default CountryPeopleLineGraph;