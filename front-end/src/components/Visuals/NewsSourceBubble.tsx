import React, { useEffect, useState, useRef } from 'react';
import axios from 'axios';
import * as d3 from 'd3';

interface ApiResponse {
    news: {
        Type: string;
    }[];
}

export async function fetchNewsSources(): Promise<string[]> {
    const url = "https://api.supportsouthsudan.me/news";
    try {
        const response = await axios.get<ApiResponse>(url);
        // Check if there are any items in the 'orgs' array
        console.log(response.data.news[0].Type)
        if (response.data.news.length > 0) {
            let results = [];
            for (let i = 0; i < response.data.news.length; i++) {
                results.push(response.data.news[i].Type)
            }
            return results;
        } else {
            console.error("No orgs items found in the response.");
            return [];
        }
    } catch (error) {
        console.error("Error fetching orgs sources:", error);
        return [];
    }
}

export function countSources(sources: string[]): Map<string, number> {
    const sourceMap = new Map<string, number>();
    sources.forEach(source => {
        const count = sourceMap.get(source) || 0;
        sourceMap.set(source, count + 1);
    });
    console.log(sourceMap.get("News and Press Release"))
    return sourceMap;
}

interface BarChartData {
    source: string;
    count: number;
}

export async function createBarChart(data: BarChartData[]): Promise<SVGSVGElement> {
    const margin = { top: 20, right: 0, bottom: 30, left: 40 };
    const width = 1700 - margin.left - margin.right;
    const height = 1000 - margin.top - margin.bottom;

    // Create SVG element
    const svg = d3.create("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .attr("viewBox", [0, 0, width, height])
        .attr("style", "max-width: 100%; height: auto;");

    // Define scales
    const x = d3.scaleBand()
        .domain(data.map(d => d.source))
        .range([margin["left"], width - margin["right"]])
        .padding(0.1);

    const y = d3.scaleLinear()
        .domain([0, d3.max(data, d => d.count)!]) // Added '!' to ensure non-null
        .range([height - margin["bottom"], margin["top"]]);

    // Draw bars
    svg.append("g")
        .attr("fill", "steelblue")
        .selectAll()
        .data(data)
        .join("rect")
        .attr("x", d => x(d.source)!)
        .attr("y", d => y(d.count) - 30)
        .attr("height", d => (height - y(d.count)))
        .attr("width", x.bandwidth());

    // Draw x-axis
    svg.append("g")
        .attr("transform", `translate(0,${height - margin["bottom"]})`)
        .call(d3.axisBottom(x).tickSizeOuter(0));

    // Draw y-axis
    svg.append("g")
        .attr("transform", `translate(${margin["left"]},0)`)
        .call(d3.axisLeft(y))
        .call(g => g.select(".domain").remove())
        .call(g => g.append("text")
          .attr("x", -margin["left"])
          .attr("y", 10)
          .attr("fill", "currentColor")
          .attr("text-anchor", "start")
          .text("# of Reports"));

    return svg.node() as SVGSVGElement;
}

const NewsSourceBarChart: React.FC = () => {
    const [chartLoaded, setChartLoaded] = useState(false);
    const chartNodeRef = useRef<SVGSVGElement | null>(null);

    useEffect(() => {
        const fetchAndRenderChart = async () => {
            try {
                const sources = await fetchNewsSources();
                if (!sources) {
                    console.error("No sources found.");
                    return;
                }
                const sourceMap = countSources(sources);
                const data = Array.from(sourceMap.entries()).map(([source, count]) => ({ source, count }));
                const chartNode = await createBarChart(data);
                setChartLoaded(true);

                chartNodeRef.current = chartNode;

                const container = document.getElementById("bar-chart-container"); // Changed to match the id in JSX
                if (container) {
                    container.appendChild(chartNode);
                } else {
                    console.error("Container not found.");
                }
            } catch (error) {
                console.error("An error occurred:", error);
            }
        };

        fetchAndRenderChart();

        // Cleanup function
        return () => {
            // Remove the chart when the component unmounts
            const chartNode = chartNodeRef.current;
            if (chartNode) {
                chartNode.parentNode?.removeChild(chartNode);
            }
        };
    }, []);

    return (
        <div id="bar-chart-container">
            <h1>News Source Bar Chart</h1>
            {!chartLoaded && <p>Loading chart...</p>}
        </div>
    );
}


export default NewsSourceBarChart;