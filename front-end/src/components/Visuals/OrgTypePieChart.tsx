import React, { useEffect, useState, useRef } from 'react';
import axios from 'axios';
import * as d3 from 'd3';

interface ApiResponse {
    orgs: {
        type: string;
    }[];
}

export async function fetchOrgTypes(): Promise<string[]> {
    const url = "https://api.supportsouthsudan.me/orgs?limit=1000";
    try {
        const response = await axios.get<ApiResponse>(url);
        // Check if there are any items in the 'orgs' array
        if (response.data.orgs.length > 0) {
            // Return the sources from the first item in the 'orgs' array
            let results = [];
            for (let i = 0; i < response.data.orgs.length; i++) {
                results.push(response.data.orgs[i].type)
            }
            console.log(response.data.orgs)
            return results;
        } else {
            console.error("No orgs items found in the response.");
            return [];
        }
    } catch (error) {
        console.error("Error fetching orgs sources:", error);
        return [];
    }
}


export function countTypes(types: string[]): Map<string, number> {
    const sourceMap = new Map<string, number>();
    types.forEach(type => {
        const count = sourceMap.get(type) || 0;
        sourceMap.set(type, count + 1);
    });
    return sourceMap;
}

interface PieChartData {
    type: string;
    count: number;
}

export async function createPieChart(data: PieChartData[]): Promise<SVGSVGElement> {
    const width = 500;
    const height = 500;

    const color = d3.scaleOrdinal<string>(d3.schemeCategory10);

    const pie = d3.pie<PieChartData>()
        .sort(null)
        .value(d => d.count);

    const arc = d3.arc<any>()
        .innerRadius(0)
        .outerRadius(Math.min(width, height) / 2 - 1);

    const arcs = pie(data);

    const svg = d3.create("svg")
        .attr("width", width)
        .attr("height", height)
        .attr("viewBox", [-width / 2, -height / 2, width, height])
        .attr("style", "max-width: 100%; height: auto; font: 10px sans-serif;");

    svg.append("g")
        .attr("stroke", "white")
        .selectAll("path")
        .data(arcs)
        .join("path")
        .attr("fill", (_, i) => color(i.toString())) // Assign colors based on index
        .attr("d", arc)
        .append("title")
        .text((d, i) => `${data[i].type}: ${data[i].count.toLocaleString("en-US")}`);

    svg.append("g")
        .attr("text-anchor", "middle")
        .selectAll("text")
        .data(arcs)
        .join("text")
        .attr("transform", d => `translate(${arc.centroid(d)})`)
        .call(text => text.append("tspan")
            .attr("y", "-0.4em")
            .attr("font-weight", "bold")
            .text((d, i) => data[i].type))
        .call(text => text.filter(d => (d.endAngle - d.startAngle) > 0.25).append("tspan")
            .attr("x", 0)
            .attr("y", "0.7em")
            .attr("fill-opacity", 0.7)
            .text((d, i) => data[i].count.toLocaleString("en-US")));

    return svg.node() as SVGSVGElement;
}


const OrgTypePieChart: React.FC = () => {
    const [chartLoaded, setChartLoaded] = useState(false);
    const chartNodeRef = useRef<SVGSVGElement | null>(null);

    useEffect(() => {
        const fetchAndRenderChart = async () => {
            try {
                const sources = await fetchOrgTypes();
                if (!sources) {
                    console.error("No sources found.");
                    return;
                }
                const typeMap = countTypes(sources);
                const data = Array.from(typeMap.entries()).map(([type, count]) => ({ type, count }));
                const chartNode = await createPieChart(data);
                setChartLoaded(true);

                chartNodeRef.current = chartNode;

                const container = document.getElementById("pie-chart-container");
                if (container) {
                    container.appendChild(chartNode);
                } else {
                    console.error("Container not found.");
                }
            } catch (error) {
                console.error("An error occurred:", error);
            }
        };

        fetchAndRenderChart();

        // Cleanup function
        return () => {
            // Remove the chart when the component unmounts
            const chartNode = chartNodeRef.current;
            if (chartNode) {
                chartNode.parentNode?.removeChild(chartNode);
            }
        };
    }, []);

    return (
        <div id="pie-chart-container">
            <h1>Organization Type Pie Chart</h1>
            {!chartLoaded && <p>Loading chart...</p>}
        </div>
    );
};

export default OrgTypePieChart;