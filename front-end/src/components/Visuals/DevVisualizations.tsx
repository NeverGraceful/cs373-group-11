import React, { useState, useEffect } from 'react';
import Footer from "../Header_and_Footer/Footer";
import "../Instance_Generics/InstancePage.css";
import RecipesOverTimeLineGraph from "./RecipesOverTimeLineGraph";
import AllergyByAgeBarChart from "./AllergyByAgeBarChart";
import AllergyTriggersPieChart from "./AllergyTriggersPieChart";



function DevVisualizations() {
  const [lineGraphLoaded, setLineGraphLoaded] = useState(false);
  const [pieChartLoaded, setPieChartLoaded] = useState(false);
  const [barGraphLoaded, setBarGraphLoaded] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      try {
        // Simulated async operations
        await new Promise(resolve => setTimeout(resolve, 2000)); // Simulate data fetching
        setLineGraphLoaded(true);

        await new Promise(resolve => setTimeout(resolve, 1000)); // Simulate data fetching
        setPieChartLoaded(true);

        await new Promise(resolve => setTimeout(resolve, 2000)); // Simulate data fetching
        setBarGraphLoaded(true);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, []);
  
  return (
    <div>
      <div className="container">
        {lineGraphLoaded && <RecipesOverTimeLineGraph />}
        {!lineGraphLoaded && <p>Loading line graph...</p>}

        {pieChartLoaded && <AllergyTriggersPieChart />}
        {!pieChartLoaded && <p>Loading pie chart...</p>}
      </div>
      <br />
      <div className="container">
        {barGraphLoaded && <AllergyByAgeBarChart />}
        {!barGraphLoaded && <p>Loading bar graph...</p>}
      </div>
      <br></br>
      <br></br>
      <br></br>
      <div style={{marginLeft: 100, marginRight: 100}}>
        <h2 style={{color: "white"}}>Critiques for Developer Group: SafeEats4You</h2>
        <h3 style={{color: "white"}}>What did they do well?</h3>
        <p style={{color: "white"}}>
          Their website is highly organized, aesthetic, and intuitive. We enjoy the use of great visuals to pair with 
          informative content. We had no issues in navigating the website, 
          running a number of sample queries, and retrieving highly relevant results.
          We enjoyed the use of multiple filter
          options shown on the UI to enable more complex queries if desired. 
          Finally, the group also does a great job in their seamless display of connections on instance pages.
        </p>
        <h3 style={{color: "white"}}>How effective was their RESTful API?</h3>
        <p style={{color: "white"}}>
          It was extremely effective. Firstly, the API was well-documented with a clear list of all possible 
          GET endpoints and content which can be queried for. Additionally, the documentation clarifies any additional 
          queryable parameters to be used in sorting/filtering. In our basic tests of their API and visualization usage,
          we ran into no issues retrieving necessary information.
        </p>
        <h3 style={{color: "white"}}>How well did they implement your user stories?</h3>
        <p style={{color: "white"}}>
          Our developer group implemented our user stories well and in a timely manner. The 
          communication was easy, straightforward and we ran into no issues on that front. 
        </p>
        <h3 style={{color: "white"}}>What did we learn from their website?</h3>
        <p style={{color: "white"}}>
          Their website was highly informative and we took away a lot of neat facts about allergies, cool
          restaurants in Austin, as well as quick recipes for college students.
        </p>
        <h3 style={{color: "white"}}>What can they do better?</h3>
        <p style={{color: "white"}}>
          One of their visuals, “Restaurants per allergy”, is left mostly unlabeled so it appears as a giant 
          pie chart that informs nothing. This also applies to their dev group pie chart visual. The other two 
          visualizations are labeled but not clearly, using weak font colors and small text which are hard to follow. 
          Additionally, we feel the website could benefit from a stricter color scheme as it currently uses a lot of 
          different colors, offering a childish vibe to the overall site.
        </p>
        <h3 style={{color: "white"}}>What puzzles us about their website?</h3>
        <p style={{color: "white"}}>
          One thing that puzzles us about their site is that on some instance pages, the connections don’t 
          make much sense. For example, the instance page for “Easy 5 Minute Vegan Cheese” has connection pages 
          regarding a steak restaurant and mediterranean diet, which are both heavily meat-focused and most likely 
          useless for a vegan consumer.
        </p>
      </div>
      <div>
        <Footer />
      </div>
    </div>
  );
}

export default DevVisualizations;
