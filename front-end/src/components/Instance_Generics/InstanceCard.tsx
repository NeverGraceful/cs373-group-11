import React from "react";
import { useEffect, useState } from "react";

import axios from "axios";
import NewsModelCard from "../News/NewsModelCard";
import CountryModelCard from "../Countries/CountryModelCard";
import CharityModelCard from "../Orgs/CharityModelCard";

interface InstanceData {
  name: string;
  link: string;
  inst_type: string;
  news_id: any;
}

// Define the props interface for the component
interface InstanceCardProps {
  instanceCardData: InstanceData; // Change to instanceCardData
}

//Component definition
const InstanceCard: React.FC<InstanceCardProps> = ({ instanceCardData }) => {
  const [loaded, setLoaded] = useState(false);
  const [error, setError] = useState(false);
  const [instanceData, setInstanceData] = useState(null);
  var data_name = instanceCardData.name
  if (instanceCardData.inst_type === "news"){
    data_name = instanceCardData.news_id
  }
  
  useEffect(() => {
    axios
      .get(`https://api.supportsouthsudan.me/${instanceCardData.inst_type}/${data_name}`)
      .then((response) => {
        setInstanceData(response.data);
        if (response.data.error != null){
          setError(true)
        }
        setLoaded(true);
      })
      .catch((error) => {
        console.error("Error", error);
        setError(true);
        setLoaded(true);
      });
  }, [data_name, instanceCardData.inst_type, instanceCardData]);

  if (!loaded) {
    return <h1 style={{ textAlign: "center" }}>Card Loading...</h1>;
  }

  if (error || !instanceData) {
    return <h1 style={{ textAlign: "center" }}>Connection not found</h1>;
  }

  if (loaded && !error){
    if (instanceCardData.inst_type === "news"){
      return <NewsModelCard data = {instanceData} searchQuery=""/>
    } else if (instanceCardData.inst_type === "orgs"){
      return <CharityModelCard data = {instanceData} searchQuery=""/>
    } else {
      return <CountryModelCard data = {instanceData} searchQuery=""/>
    }
  }
  
  return (
    null
  );
};

export default InstanceCard;