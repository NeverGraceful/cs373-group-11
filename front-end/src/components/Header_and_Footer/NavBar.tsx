import React from "react";
import { Link } from "react-router-dom";
import "./NavBar.css";

function NavBar() {
  // const navigate = useNavigate();

  return (
    <header style={{ color: "#d6c7bf" }}>
      <div className="container">
        <h1 className="logo">
          <Link to="/home" className="nav-link">
            SupportSouthSudan
          </Link>
        </h1>
        <nav>
          <ul>
            <li className="nav-item">
              <Link to="/search" className="nav-link">
                Search
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/about" className="nav-link">
                About
              </Link>
            </li>
            <li className="nav-item">
                <Link to="/visualizations" className="nav-link">
                  Visualizations
                </Link>
            </li>
            <li className="nav-item">
              <Link to="/devvisualizations" className="nav-link">
                Dev. Visualizations
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/countries" className="nav-link">
                Countries
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/orgs" className="nav-link">
                Organizations
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/news" className="nav-link">
                News
              </Link>
            </li>
          </ul>
        </nav>
      </div>
    </header>
  );
}

export default NavBar;
