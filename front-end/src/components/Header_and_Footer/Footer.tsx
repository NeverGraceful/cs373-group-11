import "./Footer.css";

function Footer() {
  return (
    <header style={{ color: "#d6c7bf" }}>
      <div className="container">
      <h1 className="logo" style={{ padding: "10px" }}>
          Support South Sudan
      </h1>
      <p style={{ padding: "30px" }}>
        "A refugee is someone who survived and who can create the future." -Amela Koluder
      </p>
      </div>
      
    </header>
  );
}

export default Footer;
