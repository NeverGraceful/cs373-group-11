import React from "react";
import { useNavigate } from "react-router-dom";
import { Buffer } from "buffer";

interface CardProps {
  name: string;
  code: string;
  flag_url: string;
  chart: { [key: number]: any };
  refugees: number;
  asylum_seekers: number;
  population: number;
  distance: number;
  news_titles: [number];
  news_id: [number];
  org_names: [string];
  org_short_names: [string];
}

interface CardComponentProps {
  data: CardProps;
  searchQuery: string;
}

const CountryModelCard: React.FC<CardComponentProps> = ({ data, searchQuery }) => {
  const navigate = useNavigate();

  const highlightSearchQuery = (text: string, query: string) => {
    if (!text || !query.trim()) return text;
    const regex = new RegExp(`(${query})`, 'gi');
    return text.replace(regex, '<mark>$1</mark>');
  };

  return (
    <div className="col-md-4 mb-4" data-testid="country-model-card">
      <div
        className="card p-3 border rounded-3 shadow"
        onClick={() =>
          navigate(`/countries/${data.name}`, { state: { data: data } })
        }
        style={{ width: "300px", height: "500px", backgroundColor: "#e8ddd8"  }}
      >
        <img
          src={Buffer.from(data.flag_url, 'utf8').toString()}
          className="card-img-top"
          style={{ height: "200px", objectFit: "cover" }}
          alt={data.flag_url}
        />
        <div className="card-body">
          <h5 className="card-title"><span dangerouslySetInnerHTML={{ __html: highlightSearchQuery(data.name, searchQuery) }} /></h5>
          <p className="card-text">Pop. accepted: <span dangerouslySetInnerHTML={{ __html: highlightSearchQuery(data.refugees.toString(), searchQuery) }} /></p>
          <p className="card-text">
            Number of asylum seekers: <span dangerouslySetInnerHTML={{ __html: highlightSearchQuery(data.asylum_seekers.toString(), searchQuery) }} />
          </p>
          <p className="card-text">Total pop.: <span dangerouslySetInnerHTML={{ __html: highlightSearchQuery(data.population.toString(), searchQuery) }} /></p>
          <p className="card-text">
            Distance from South Sudan: <span dangerouslySetInnerHTML={{ __html: highlightSearchQuery(data.distance.toString(), searchQuery) }} />
          </p>
        </div>
      </div>
    </div>
  );
};

export default CountryModelCard;
