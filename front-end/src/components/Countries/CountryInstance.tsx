import React from "react";
import { useEffect, useRef } from "react";
import { useLocation } from "react-router-dom";
import Chart from "chart.js/auto";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import "../Instance_Generics/InstancePage.css";
import InstanceCard from "../Instance_Generics/InstanceCard";

export interface CountryInstanceData {
  name: string;
  code: string;
  flag_url: string;
  chart: { [key: number]: any };
  refugees: number;
  asylum_seekers: number;
  population: number;
  distance: number;
  news_titles: [string];
  news_id: [number];
  org_names: [string];
  org_short_names: [string];
}

const CountryInstance = () => {
  const location = useLocation();
  const canvasRef = useRef<HTMLCanvasElement>(null);

  interface ChartData {
    [year: string]: [number, number];
  }

  useEffect(() => {
    if (canvasRef.current && location.state?.data) {
      const ctx = canvasRef.current.getContext("2d");
      if (ctx) {
        const chartData: ChartData = location.state.data.chart;
        const years = Object.keys(chartData);
        const refugeeData = years.map((year) => chartData[year][0]);
        const asylumSeekerData = years.map((year) => chartData[year][1]);

        const doubleBarChart = new Chart(ctx, {
          type: "bar",
          data: {
            labels: years,
            datasets: [
              {
                label: "Refugees Received",
                data: refugeeData,
                backgroundColor: "rgba(255, 99, 132, 0.5)",
              },
              {
                label: "Asylum Seekers",
                data: asylumSeekerData,
                backgroundColor: "rgba(54, 162, 235, 0.5)",
              },
            ],
          },
          options: {
            plugins: {
              title: {
                display: true,
                text: location.state.data.name,
              },
            },
          },
        });

        return () => {
          doubleBarChart.destroy();
        };
      }
    }
  }, [location.state?.data, location.state.data.name]);

  return (
    <div className="container mt-4">
      <h1 className="title text-center mb-4">{location.state.data.name}</h1>
      <div className="row">
        <div className="col-md-6">
          <div className="description-box mb-4">
          </div>
        </div>
        <div className="col-md-6">
          <div className="data-box mb-4">
            <div className="data-row">
              <span className="data-label">Population accepted refugees: </span>
              <span className="data-value">{location.state.data.refugees}</span>
            </div>
            <div className="data-row">
              <span className="data-label">Population asylum seekers: </span>
              <span className="data-value">
                {location.state.data.asylum_seekers}
              </span>
            </div>
            <div className="data-row">
              <span className="data-label">Total country population: </span>
              <span className="data-value">
                {location.state.data.population}
              </span>
            </div>
            <div className="data-row">
              <span className="data-label">Distance from South Sudan: </span>
              <span className="data-value">{location.state.data.distance}</span>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-md-12 mb-4">
            <img
              src={location.state.data.flag_url}
              alt="Country"
              className="img-fluid smaller-image"
            />
            <canvas
              ref={canvasRef}
              id="doubleBarChart"
              width="30"
              height="20"
            ></canvas>
          </div>
        </div>
      </div>
      <div></div>
      <div className="row" style={{ marginTop: "150px" }}>
        <div className="row-md-6">
          <div className="data-row">
            <span className="data-label">News: </span>
            <Row>
              {location.state.data.news_id && location.state.data.news_id.slice(0,5) && location.state.data.news_titles &&
                location.state.data.news_titles
                  .slice(0, 5)
                  .map((news: string, index: number) => {
                    const trimmed = news.trim();
                    
                    if (trimmed) {
                      return (
                        <Col key={index} xs={6} md={4} lg={4}>
                          <InstanceCard
                            instanceCardData={{
                              name: trimmed,
                              link: `../../news/${location.state.data.news_id[index]}`,
                              inst_type: "news",
                              news_id: location.state.data.news_id[index]
                            }}
                          />
                        </Col>
                      );
                    } else {
                      return (
                        <p>Error</p>
                      )
                    } 
                  })}
              {location.state.data.news_titles.length === 0 &&
                <Col>
                  <h1 style={{ textAlign: "center" }}>Connection not found</h1>;
                </Col>
              }
            </Row>
          </div>
        </div>
        <div className="row-md-6">
          <div className="data-row">
            <span className="data-label">Orgs: </span>
            <Row>
              {location.state.data.org_names &&
                location.state.data.org_names
                  .slice(0, 5)
                  .map((org: string, index: number) => {
                    const trimmed = org.trim();
                    if (trimmed) {
                      return (
                        <Col key={index} xs={6} md={4} lg={3}>
                          <InstanceCard
                            instanceCardData={{
                              name: trimmed,
                              link: `../../orgs/${trimmed}`,
                              inst_type: "orgs",
                              news_id: -1
                            }}
                          />
                        </Col>
                      );
                    } else {
                      return (
                        <p>Error</p>
                      )
                    } 
                  })}
              {location.state.data.org_names.length === 0 &&
                <Col>
                  <h1 style={{ textAlign: "center" }}>Connection not found</h1>;
                </Col>
              }
            </Row>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CountryInstance;
