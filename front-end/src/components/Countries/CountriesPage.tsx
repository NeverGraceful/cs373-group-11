import React from "react";
import { useState, useEffect } from "react";
import CountryModelCard from "./CountryModelCard";
import Footer from "../Header_and_Footer/Footer";

import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";

import axios from "axios";

interface CountryInstance {
  name: string;
  code: string;
  flag_url: string;
  chart: { [key: number]: any };
  refugees: number;
  asylum_seekers: number;
  population: number;
  distance: number;
  news_titles: [number];
  news_id: [number];
  org_names: [string];
  org_short_names: [string];
}

function CountriesPage() {
  const [searchQuery, setSearchQuery] = useState("");
  const [sortBy, setSortBy] = useState("");
  const [totalItems, setTotalItems] = useState(0);

  const numCardsPerPage = 8;
  const [currentPage, setCurrentPage] = useState(1);
  const [loaded, setLoaded] = useState(false);
  const [totalPages, setTotalPages] = useState(0)
  const [payload, fillPayload] = useState("")
  const [countryInstances, setCountryInstances] = useState<CountryInstance[]>(
    []
  );

  const handleSearch = async (event: React.FormEvent) => {
    event.preventDefault();

    fillPayload(`&search=${encodeURIComponent(searchQuery)}&sort=${sortBy}`)
    setCurrentPage(1);
  };

  const nextPage = () => {
    setCurrentPage((prevPage) => Math.min(prevPage + 1, totalPages));
  };

  const prevPage = () => {
    setCurrentPage((prevPage) => Math.max(prevPage - 1, 1));
  };

  const goToPage = (pageNumber: number) => {
    setCurrentPage(pageNumber);
  };

  useEffect(() => {
    axios
      .get(
        `https://api.supportsouthsudan.me/countries?page=${currentPage}&limit=${numCardsPerPage}${payload}`
      )
      .then((response) => {
        setTotalItems(response.data._metadata.total);
        setTotalPages(Math.ceil(totalItems/numCardsPerPage));
        setCountryInstances(response.data.countries);
      })
      .catch((error) => {
        console.log("Error", error);
      });
      setLoaded(true); 
  }, [currentPage, payload, totalItems]);

  if (!loaded) {
    return (
      <h1 style={{ textAlign: "center", color: "#e8ddd8" }}>Page Loading...</h1>
    );
  }

  return (
    <div>
      <div className="container mt-5" style={{ color: "#e8ddd8" }}>
        <h1 className="title text-center mb-4" style={{ color: "#FF4928" }}>
          Countries
        </h1>
        <p>
          A comprehensive collection of information about the countries that
          have accepted South Sudanese refugees. It includes details such as the
          name of the country, its geographical distance from Sudan, and
          population demographics. The Countries model serves to contextualize
          the refugee situation by highlighting the global response and
          providing insights into the social, political, and economic
          environments where South Sudanese refugees seek asylum.
        </p>
        <form
          onSubmit={handleSearch}
          className="search-form"
          style={{ margin: "20px auto", maxWidth: "1100px" }}
        >
          <div className="input-group mb-3">
            <input
              type="text"
              className="form-control"
              placeholder="Search..."
              value={searchQuery}
              onChange={(e) => setSearchQuery(e.target.value)}
              style={{ flex: "4" }}
            />
            <select
              className="form-select"
              value={sortBy}
              onChange={(e) => setSortBy(e.target.value)}
              style={{ flex: "1" }}
            >
              <option value="">Sort by</option>
              <option value="name">Name</option>
              <option value="refugees">Number Refugees</option>
              <option value="asylum_seekers">Number Asylum Seekers</option>
              <option value="population">Population Accepted</option>
              <option value="distance">Distance from South Sudan</option>
            </select>
            <button
              type="submit"
              className="btn btn-secondary"
              style={{
                backgroundColor: "#60755a",
                borderColor: "#60755a",
                color: "white",
              }}
            >
              Search
            </button>
          </div>
        </form>
        <p data-testid="instance-counter">
          - Instances in this model: {totalItems}
        </p>
        <p>- Number of pages {totalPages}</p>
        <p>- Current page: {currentPage}</p>
        <br />
        <Row className="justify-content-center">
          {countryInstances &&
            countryInstances.map((instance) => {
              return (
                <Col className="mb-3" key={instance.code}>
                  <CountryModelCard data={instance} searchQuery={searchQuery}/>
                </Col>
              );
            })}
        </Row>
      </div>
      <div style={{ textAlign: "center", marginBottom: "20px" }}>
        <Button
          variant="dark"
          onClick={prevPage}
          disabled={currentPage === 1}
          style={{ marginRight: "10px" }}
        >
          Prev
        </Button>
        {Array.from({ length: totalPages }, (_, i) => i + 1).map(
          (pageNumber) => (
            <Button
              key={pageNumber}
              variant={currentPage === pageNumber ? "light" : "light"}
              style={
                currentPage === pageNumber
                  ? { backgroundColor: "lightgray", marginRight: "10px" }
                  : { backgroundColor: "white", marginRight: "10px" }
              }
              onClick={() => goToPage(pageNumber)}
            >
              {pageNumber}
            </Button>
          )
        )}
        <Button
          variant="dark"
          onClick={nextPage}
          disabled={currentPage === totalPages}
          style={{ marginLeft: "10px" }}
        >
          Next
        </Button>
      </div>
      <div>
        <Footer />
      </div>
    </div>
  );
}

export default CountriesPage;
