import axios from 'axios';

//write function that can iterate through data and count commits and count issues

export interface Member {
  name: string;
  aliases: string;
  major: string;
  bio: string;
  photo: any;
  role: string;
  commits: number; // fetched from API
  issues: number; // fetched from API
  tests: number;
}

export let members: Array<Member> = [
  {
    name: "Jean-Claude Bissou",
    aliases: "Jean-Claude Bissou",
    major: "Computer Science",
    bio: "I'm a sophomore at UT and my hobbies include gaming, modeling, and playing basketball!",
    photo: "jc_photo.jpg",
    role: "Back-End",
    commits: 0,
    issues: 0,
    tests: 8
  },
  {
    name: "Anna Jimenea",
    aliases: "Anna Jimenea, Anna, NeverGraceful",
    major: "Computer Science",
    bio: "I am currently a junior majoring in Computer Science. In my free time, I play video games and crochet.",
    photo: "anna_photo.jpg",
    role: "Front-End",
    commits: 0,
    issues: 0,
    tests: 8
  },
  {
    name: "Sugandha Kumar",
    aliases: "Sugandha Kumar",
    major: "Computer Science",
    bio: "I'm a junior Computer Science major at UT Austin and some of my hobbies include weightlifting, gaming, and cooking.",
    photo: "sugandha_pic.png",
    role: "Back-End",
    commits: 0,
    issues: 0,
    tests: 8
  },
  {
    name: "Benjamin Sandoval",
    aliases: "Benjamin Sandoval, Benjamin P Sandoval",
    major: "Computer Science",
    bio: "I'm a sophomore at UT Austin and I enjoy cooking and watching sports.",
    photo: "Ben_pic.jpg",
    role: "Front-End",
    commits: 0,
    issues: 0,
    tests: 8
  },
  {
    name: "Adithya Bhonsley",
    aliases: "Adithya Bhonsley",
    major: "Computer Science",
    bio: "I'm a senior at UT Austin studying computer science and I enjoy watching and playing sports.",
    photo: "ab_photo.jpg",
    role: "Back-End",
    commits: 0,
    issues: 0,
    tests: 9
  }
];

// Create a cancel token source
let cancelTokenSource: any = null;
// const cancelTokenSource = axios.CancelToken.source();

//will have to update methods to account for API pagination
//updates members array with commit data from API
export async function updateAllCommits(): Promise<number> {
  clearCommits();
  let commits: number = 0;
  let page: number = 1;
  // Create a new cancel token source
  cancelTokenSource = axios.CancelToken.source();
  try {
    do {
      const response = await axios.get('https://gitlab.com/api/v4/projects/55997759/repository/commits', {
        params: {
          page: page,
          per_page: 100,
          ref_name: "develop"
        },
        // cancelToken: cancelTokenSource.token // Pass the cancel token
      });
      response.data.forEach((commit: { author_name: string; }) => updateCommit(commit));
      commits += response.data.length;
      // members.forEach(member => {
      //   commits = commits + member.commits
      // });
      page++;
    } while(commits % 100 === 0);
  } catch (error) {
    // console.error(error);
  }
  return commits;
}

//update appropriate stats given a commit
export function updateCommit(commit: { author_name: string; }){
  members.forEach(member => {
    if(member.aliases.includes(commit.author_name)){
      member.commits++;
    }
  });
}

//to be implemented
export async function updateAllIssues(): Promise<number> {
  clearIssues();
  let issues: number = 0;
  let page: number = 1;
  try {
    do{
      const response = await axios.get('https://gitlab.com/api/v4/projects/55997759/issues', {
        params:{
          page: page,
          per_page: 100,
          order_by: "updated_at",
          state: "closed"
        },
        // cancelToken: cancelTokenSource.token // Pass the cancel token
      });
      response.data.forEach((issue: { assignee: { name: string; } }) => updateIssue(issue));
      issues += response.data.length;
      // members.forEach(member => {
      //   issues += member.issues
      // });
      page++;
    }while(issues % 100 === 0);
  } catch (error) {
    // console.error(error);
  }
  return issues;
}

// let cancelTokenSource: any = axios.CancelToken.source();

// Cancel the Axios requests when necessary
export function cancelRequests() {
  cancelTokenSource?.cancel('Operation canceled by the user.');
}

function updateIssue(issue: { assignee: { name: string; } }){
  members.forEach(member => {
    if(issue.assignee && member.aliases.includes(issue.assignee.name)){
      member.issues++;
    }
  });
}

export function clearIssues(){
  members.forEach(member => {
    member.issues = 0;
  });
}

export function clearCommits(){
  members.forEach(member => {
    member.commits = 0;
  });
}


